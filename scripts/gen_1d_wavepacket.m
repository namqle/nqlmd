clear variables;
close all;

%% Inputs

% Domain length
NZ = 1000;

% Wave packet settings
A = 0.1;
q0 = 0.2;
r0 = 400;
xi = 20;

% Lattice constant (two atoms per 1d cell)
a = 5.405;

filename = '1d.initial';

%% Allocate variables

N = 2*NZ;

id   = zeros(N, 1);
type = zeros(N, 1);
mass = zeros(N, 1);
pos  = zeros(N, 1);
vel  = zeros(N, 1);

%% Populate variables

% IDs
id(:) = (1:N)';

% Masses
mass(:) = 39.948;

% Positions
for i = 1:N
  pos(i) = 0.5*(i-1);
end
pos = pos+0.25;

pos = pos*a;

% Velocities
vel(:) = 0;

% Types
type(:) = 1;
type(pos > 0.5*(NZ*a)) = 2;

%% Add a wave packet
A = A * a;
q0 = q0 * 2*pi/a;
xi = xi * a;
r0 = (r0+0.25) * a;

k = 0.1016;
m = 39.948;
w0 = (4*k/m)^0.5 * sin(q0*(a/2)/2);
w0 = w0 * 98.2291;

dr = A * exp(1i*q0 * (pos(:)-r0));... .* exp(-(pos(:)-r0).^2 / xi^2);
dv = -1i*w0 * dr;

pos = pos + real(dr);
vel = vel + real(dv);

%% Debug: plot

figure(1);
plot(1:N, real(dr), 'o');
% figure(2);
% plot(1:N, real(dv));

%% Write initial configuration to file

fid = fopen(filename, 'w');

% Header
fprintf(fid,'# Initial configuration generated %s\n',datestr(datevec(now),0));

% Number of atoms
fprintf(fid, '%d\n', N);

% Atom data
full_array = [id type mass pos vel];
% fprintf(fid, '# id type mass z vz (%d atoms)\n', size(id,1));
fprintf(fid, '%d %d %f %f %f\n', full_array');
