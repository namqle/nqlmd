clear variables;
close all;

%% Settings

filename = 'example02.micro';
nfields = 13;

%% Manual plot settings
X = 4;
Y = 4;

% Axes
set(0, 'DefaultAxesBox', 'on');
set(0, 'DefaultAxesLineWidth', 0.5);
set(0, 'DefaultAxesXMinorTick', 'on');
set(0, 'DefaultAxesYMinorTick', 'on');
set(0, 'DefaultAxesTickDir', 'in');

% Plots
set(0, 'DefaultLineLineWidth', 1.5);
set(0, 'DefaultLineMarkerSize', 4);

% Figures
set(0, 'DefaultFigurePaperSize', [X Y]);
set(0, 'DefaultFigurePaperPositionMode', 'manual');
set(0, 'DefaultFigurePaperPosition', [ 0 0 X Y ] );
set(0, 'DefaultFigurePosition', [ 0 0 X*72 Y*72 ] );

% Comment here to switch on/off Times font
set(0, 'DefaultTextFontname', 'Times New Roman', ...
       'DefaultTextFontsize', 10, ...
       'DefaultAxesFontname', 'Times New Roman', ...
       'DefaultAxesFontsize', 10 );



%% Read

raw = load(filename);
N = max(raw(:,1)) + 1;

nframes = numel(raw) / N / nfields;
data = reshape(raw, [N nframes nfields]);

m = data(:,:,3);
z = data(:,:,7);
v = data(:,:,8:10);


%% Compute temperature of each atom from 1/2 mv^2

T = m .* sum(v.^2, 3) / 3; % in amu (A/ps)^2 * (boltzmann constant)
T = T * 1.20267; % in K


%% Bin by unit cells along z direction

a = 5.405;
nbins = round(max(max(z)) / a);

T_binned = zeros([nbins nframes]);
n_binned = zeros([nbins nframes]);

for bin = 1 : nbins
  zlo = (bin-1)*a;
  zhi = bin*a;

  n_binned(bin,:) = sum(z>zlo & z<zhi, 1);
  T_binned(bin,:) = sum(T .* (z>zlo & z<zhi), 1) ./ n_binned(bin,:);
end


%% Average over time, after some "equilibration" period

start_step = 20;

num_ave_frames = nframes - start_step + 1;

T_profile = sum(T_binned(start_step:end, :), 2) / num_ave_frames;
