
import numpy as np




##
## Settings
##

# Lattice constant
a = 5.405

# Domain size in unit cells
box = np.array([5, 5, 40])

# Atomic basis
basis = 0.5 * np.array([ [0, 0, 0],
                         [1, 1, 0],
                         [1, 0, 1],
                         [0, 1, 1] ])
offset = 0.25

# Initial "temperature"
kB = 0.83148 # (amu (a/ps)^2) / K
T = 40 # K

filename = 'crystal.init'



##
## Calculations
##

Nbasis = basis[:,0].size         # number of basis atoms
Natoms = Nbasis * box.prod()     # number of atoms
Ndim   = box.size                # number of dimensions
if (basis[0,:].size != Ndim):
  print("ERROR: box dim doesn't match basis dim")
  exit()

# IDs
atom_id = np.array( range(Natoms) )

# Types
atom_type = np.ones(Natoms)

# Masses
atom_m = 40 * np.ones(Natoms)

# Positions
atom_r = np.zeros([Natoms, Ndim])

atom_r = np.array( range(Natoms) )
atom_r = np.tile(atom_r, [Ndim, 1])
atom_r = np.transpose(atom_r)

cycle_length = Nbasis
for dim in xrange(Ndim):
  atom_r[:,dim] = (atom_r[:,dim] / cycle_length) % box[dim]
  cycle_length  *= box[dim]

atom_r = atom_r + np.tile(basis, [box.prod(), 1]) # Add basis on top

print("Domain in unit cells:")
print(box)

print("\nBasis atoms:")
print(basis)

print("\nNormalized coords of {0} atoms:".format(Natoms))
print(atom_r)

atom_r[:,:] += offset;
atom_r[:,:] *= a;

# Velocities
atom_v = np.zeros([Natoms, Ndim])
v_abs  = np.zeros(Natoms)

v_abs[:] = np.sqrt(3*kB*T / atom_m[:])
if (Ndim == 3):
  theta = np.pi * np.random.uniform(0, 1, v_abs.shape)
  phi = 2*np.pi * np.random.uniform(0, 1, v_abs.shape)

  atom_v[:,0] = v_abs[:] * np.sin(theta[:]) * np.cos(phi[:])
  atom_v[:,1] = v_abs[:] * np.sin(theta[:]) * np.sin(phi[:])
  atom_v[:,2] = v_abs[:] * np.cos(theta[:])
elif (Ndim == 2):
  theta = 2*np.pi * np.random.uniform(0, 1, v_abs.shape)

  atom_v[:,0] = v_abs[:] * np.cos(theta[:])
  atom_v[:,1] = v_abs[:] * np.sin(theta[:])
elif (Ndim == 1):
  orientation = (np.random.random_integers(0, 1, v_abs.shape) - 0.5) * 2

  atom_v[:,0] = v_abs[:] * orientation[:]



##
## Write to file
##

# headers
f = open(filename, "w")
from datetime import datetime
f.write("# Initial config generated " + str(datetime.now()) + "\n")
# f.write("# id, type, m, r(dim), v(dim)\n")
f.write("{0}\n".format(Natoms))
f.close()

# actual configuration data
f = open(filename, "a")
data = np.column_stack((atom_id, atom_type, atom_m, atom_r, atom_v))

formatstr = "%d %d %f";
for i in xrange(Ndim):
  formatstr += " %f %f"

np.savetxt(f, data, formatstr)
f.close()

# report success
print("\nWrote final coords to the file " + filename)
print("  with offset = {0}, a = {1}".format(offset, a))




##
## For debugging: plot
##

# import numpy as np
# from mpl_toolkits.mplot3d import Axes3D
# import matplotlib.pyplot as plt
# 
# fig = plt.figure()
# ax  = Axes3D(fig)
# 
# ax.scatter(atom_r[:,0], atom_r[:,1], atom_r[:,2])
# 
# plt.show()
