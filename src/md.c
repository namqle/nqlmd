/*
 *  md.c
 *  nqlmd
 *
 *  Created by Nam Quang Le on 11/14/12.
 *  Copyright 2012 Nam Q. Le (University of Virginia). All rights reserved.
 *
 */

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include "common.h"
#include "forces.h"
#include "routine.h"

#include "md.h"

int md(Sim *s) {
  
  /********/
  /* PREP */
  /********/
  // Initial force calculation
  s->start_forces = clock();
  if ( forces(s) == RETURN_ERROR ) {
    fprintf(stderr, "%s: ABORTED FORCE CALC -- error getting forces on step <%d>\n", __func__, s->step);
    return RETURN_ERROR;
  }
  s->time_forces += (clock() - s->start_forces) / CLOCKS_PER_SEC;

  /*************/
  /* TIME LOOP */
  /*************/
  fprintf(s->flog, "\n%sSTARTING SIMULATION\n", s->log_msg_prefix);
  
  int stop_sim = 0;
  while ( !stop_sim ) {
    
    /************/
    /* ROUTINES */
    /************/
    for (int routine_id=0; routine_id < MAX_ROUTINES; routine_id++) {

      // For each routine, call routine_run
      int outcome = routine_run(s, routine_id);
      if ( outcome == RETURN_ERROR ) {
        fprintf(stderr, "%s: ABORTED ROUTINE -- error running routine \n", __func__);
        return RETURN_ERROR;
      }
      else if ( outcome == RETURN_STEADY_STATE ) {
        fprintf(s->flog, "%sStopping at step %d: thermal steady state reached\n", s->log_msg_prefix, s->step);
        stop_sim = 1;
      }
    }

    // Is it the end of this run? Either "run_until max_step" or "run_until steady"
    if ((s->step >= s->max_step) && (s->max_step != MAX_STEP_STEADY)){
      stop_sim = 1;
    }

    s->step++;
    s->total_steps++;
  }

  return RETURN_SUCCESS;
}
