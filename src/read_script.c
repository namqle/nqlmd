/*
 *  read_script.c
 *  nqlmd
 *
 *  Created by Nam Quang Le on 11/14/12.
 *  Copyright 2012 Nam Q. Le (University of Virginia). All rights reserved.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "common.h"
#include "routine.h"
#include "read_script.h"

// Prototypes
int tokenize(char *buffer, char key[], char args[][MAX_SCRIPT_ARG_LENGTH]);
int read_initial_config(Sim *s, char *filename);



// READ_SCRIPT
int read_script(Sim *s) {
  
  // On the first time, print a little header
  if (s->step == 0) {
    fprintf(s->flog, "%sInitializing: jobname <%s>\n", s->log_msg_prefix, s->jobname);
    time_t timer;
    if ( (timer = time(NULL)) == -1 ) {
      fprintf(stderr, "%s: WARNING -- Error getting time\n", __func__);
      fprintf(s->flog, "%sError getting start time\n\n", s->log_msg_prefix);
    }
    else {
      fprintf(s->flog, "%sLog opened %s\n", s->log_msg_prefix, asctime(localtime(&timer)));
    }
  }

  // Parse the script line-by-line
  char buffer[MAX_LINE_LENGTH];
  char key[MAX_SCRIPT_ARG_LENGTH];
  char args[MAX_SCRIPT_ARGS][MAX_SCRIPT_ARG_LENGTH];
  int num_args = 0;
  int num_args_expected = 0;
  
  while ( fgets(buffer, MAX_LINE_LENGTH, s->fscript) != NULL ) {
    
    // Break the buffer line into a keyword + arguments
    char input_line[MAX_LINE_LENGTH];
    strcpy(input_line, buffer);
    num_args = tokenize(buffer, key, args);
    
    /* Echo input */
    //   Echo comments and whitespace directly
    if ( key[0]=='#' || !strcmp(key, "") ) {
      fprintf(s->flog, "%s", input_line);
      continue;
    }
    else { // Mark other lines as program input
      fprintf(s->flog, "%s%s", s->log_echo_prefix, input_line);
    }
    
    /***********************/
    /* SCRIPT OPTION TREE! */
    /***********************/
    
    /*************/
    /* sim_units */
    /*************/
    //   Set unit system
    if (!strcmp(key, "sim_units")) {
      num_args_expected = 1;
      if (num_args == num_args_expected) {
        
        // "Switch" according to unit system
        if (!strcmp(args[0], "default") || !strcmp(args[0], "aap")) {
          strcpy(s->units, "aap");
          fprintf(s->flog, "%sUnits <%s> (M=amu, L=angstrom, T=ps, E=eV, F=eV/angstrom)\n", s->log_msg_prefix, s->units);
        }
        else if (!strcmp(args[0], "si")) {
          strcpy(s->units, "si");
          fprintf(s->flog, "%sUnits <%s> (M=kg, L=m, T=s, E=J, F=N=J/m)\n", s->log_msg_prefix, s->units);
        }
        else {
          fprintf(stderr, "%s: BAD UNITS -- don't recognize unit system <%s>\n", __func__, args[0]);
          return RETURN_ERROR;
        }


      }
      else {
        fprintf(stderr, "%s: BAD ARGS -- sim_units expects %d arg, got %d\n", __func__, num_args_expected, num_args);
        return RETURN_ERROR;
      }
    }
    
    /***********/
    /* sim_dim */
    /***********/
    //   Set the number of dimensions
    else if (!strcmp(key, "sim_dim")) { 
      num_args_expected = 1;
      
      if (num_args == num_args_expected) {
        
        if (!strcmp(args[0], "1") || !strcmp(args[0], "2") || !strcmp(args[0], "3")) {
          s->sim_dim = strtod(args[0], NULL);
          fprintf(s->flog, "%sDimensions set to <%d>\n", s->log_msg_prefix, s->sim_dim);
        }
        else {
          fprintf(stderr, "%s: BAD ARGS -- sim_dim expects 1/2/3, got %s\n", __func__, args[0]);
          return RETURN_ERROR;
        }
      }
      else {
        fprintf(stderr, "%s: BAD ARGS -- sim_dim expects %d arg, got %d\n", __func__, num_args_expected, num_args);
        return RETURN_ERROR;
      }
    }
    
    /***********/
    /* sim_box */
    /***********/
    //   Set the box size
    else if (!strcmp(key, "sim_box")) {
      if (s->units[0] == '\0') {
        fprintf(stderr, "%s: MUST SET UNITS -- must set sim_units before sim_box\n", __func__);
        return RETURN_ERROR;
      }
      if (s->sim_dim!=1 && s->sim_dim!=2 && s->sim_dim!=3) {
        fprintf(stderr, "%s: MUST SET DIM -- must set sim_dim to 1/2/3 before sim_box\n", __func__);
        return RETURN_ERROR;
      }
      
      num_args_expected = 2 * s->sim_dim;
      
      if (num_args == num_args_expected) {
        for (int i=0; i<num_args_expected; i++) {
          s->sim_box[i] = strtof(args[i], NULL);
        }
        fprintf(s->flog, "%sBox sizes set: x = (%g, %g), y = (%g, %g), z = (%g, %g)\n", s->log_msg_prefix, \
                s->sim_box[0], s->sim_box[1], s->sim_box[2], s->sim_box[3], s->sim_box[4], s->sim_box[5]);
        // Populate derived quantities
        for (int dim=0; dim < (s->sim_dim); dim++) {
          if (s->sim_box[2*dim+1] > s->sim_box[2*dim]) {
            s->sim_box_L[dim] = (s->sim_box[2*dim+1]) - (s->sim_box[2*dim]);
            s->sim_box_halfL[dim] = 0.5 * (s->sim_box_L[dim]);
          }
          else {
            fprintf(stderr, "%s: BAD BOUNDS IN DIM %d --box bounds must be given as <min max>\n ", __func__, dim+1);
            return RETURN_ERROR;
          }
        }
      }
      else {
        fprintf(stderr, "%s: BAD ARGS -- sim_box expects %d arg, got %d\n", __func__, num_args_expected, num_args);
        return RETURN_ERROR;
      }
    }
    
    /***********/
    /* sim_pbc */
    /***********/
    //   Set periodic boundary conditions
    else if (!strcmp(key, "sim_pbc")) {
      if (s->sim_dim!=1 && s->sim_dim!=2 && s->sim_dim!=3) {
        fprintf(stderr, "%s: MUST SET DIM -- must set sim_dim to 1/2/3 before sim_pbc\n", __func__);
        return RETURN_ERROR;
      }
      
      num_args_expected = s->sim_dim;
      
      if (num_args == num_args_expected) {
        for (int i=0; i<num_args_expected; i++) {
          s->sim_pbc[i] = strtod(args[i], NULL);
        }
        fprintf(s->flog, "%sPeriodic boundary conditions set: (x,y,z) -> (%d,%d,%d)\n", s->log_msg_prefix, \
                s->sim_pbc[0], s->sim_pbc[1], s->sim_pbc[2]);
      }
      else {
        fprintf(stderr, "%s: BAD ARGS -- sim_pbc expects %d arg, got %d\n", __func__, num_args_expected, num_args);
        return RETURN_ERROR;
      }
    }
    
    /*************/
    /* init_file */
    /*************/
    //   Read initial atom config from a data file
    else if (!strcmp(key, "init_file")) {
      if (s->units[0] == '\0') {
        fprintf(stderr, "%s: MUST SET UNITS -- must set sim_units before reading init_file\n", __func__);
        return RETURN_ERROR;
      }
      if (s->sim_dim!=1 && s->sim_dim!=2 && s->sim_dim!=3) {
        fprintf(stderr, "%s: MUST SET DIM -- must set sim_dim to 1/2/3 before reading init_file\n", __func__);
        return RETURN_ERROR;
      }
      
      num_args_expected = 1;
      
      if (num_args == num_args_expected) {
        if (read_initial_config(s, args[0]) ){ // read the file here
          fprintf(stderr, "%s: BAD INPUT -- error reading initial configuration\n", __func__);
          return RETURN_ERROR;
        }
        fprintf(s->flog, "%sRead %d atoms, of %d types, from <%s>\n", s->log_msg_prefix, s->N, s->num_types, args[0]);
      }
      else {
        fprintf(stderr, "%s: BAD ARGS -- init_file expects %d arg, got %d\n", __func__, num_args_expected, num_args);
        return RETURN_ERROR;
      }

      // Group 0 is reserved to include all atoms
      s->group_num_atoms[0] = s->N;
      for (int n=0; n < s->N; n++) {
        s->group[0][n] = n;
      }

    }
    
    /*********/
    /* group */
    /*********/
    //   Look inside some region (only once, before the simulation starts) and put those atoms into a group
    else if (!strcmp(key, "group")) {
      if (s->N==0) {
        fprintf(stderr, "%s: NEED INITIAL CONFIG -- must read init_file before setting %s\n", __func__, key);
        return RETURN_ERROR;
      }
      
      num_args_expected = 1 + 2*(s->sim_dim);
      
      if (num_args == num_args_expected) {
        // Get the group identifier
        int group_id = strtol(args[0], NULL, 10);
        if (group_id == 0) {
          fprintf(stderr, "%s: BAD GROUP ID -- group id 0 is reserved for 'all atoms'\n", __func__);
          return RETURN_ERROR;
        }
        
        // Populate the group: look at each atom...
        int num_atoms_in_group = 0;
        for (int n=0; n < s->N; n++) {
          // Check to see if the atom is inside the box...
          int is_inside = 1;
          for (int dim=0; dim < s->sim_dim; dim++) {
            double min = strtod(args[1 + 2*dim], NULL);
            double max = strtod(args[2 + 2*dim], NULL);
            if (max < min) {
              fprintf(stderr, "%s: BAD GROUP BOX -- min > max (%f > %f) in dim %d\n", __func__, min, max, dim);
              return RETURN_ERROR;
            }
            if ((s->atom_r[n][dim] < min) || (s->atom_r[n][dim] > max)) {
              is_inside = 0;
            }
          }
          // If so, then add it to the group
          if (is_inside) {
            s->group[group_id][num_atoms_in_group] = n;
            num_atoms_in_group++;
          }
        }
        
        // Done with this group
        if (num_atoms_in_group == 0) {
          fprintf(stderr, "%s: BAD GROUP BOX -- no atoms found in the given region\n", __func__);
          return RETURN_ERROR;
        }
        else {
          s->group_num_atoms[group_id] = num_atoms_in_group;
          fprintf(s->flog, "%s%d atoms assigned to group id %d\n", s->log_msg_prefix, s->group_num_atoms[group_id], group_id);
        }
      }
      else {
        fprintf(stderr, "%s: BAD ARGS -- group expects %d arg, got %d\n", __func__, num_args_expected, num_args);
        return RETURN_ERROR;
      }
    }
    
    /*************/
    /* pot_style */
    /*************/
    //   Set potential style between atom types
    else if (!strcmp(key, "pot_style")) {
      if (s->N==0) {
        fprintf(stderr, "%s: NEED INITIAL CONFIG -- must read init_file before setting %s\n", __func__, key);
        return RETURN_ERROR;
      }
      
      num_args_expected = 3;
      
      if (num_args == num_args_expected) {
        int type1 = strtod(args[0], NULL);
        int type2 = strtod(args[1], NULL);
        if ( type1 <= type2 ) {
          int pot_id = (type1-1)*(2*(s->num_types)-type1)/2 + type2;
          if ( !strcmp(args[2], POT_STYLE_NAME_HARMONIC) ) {
            s->pot_style[pot_id] = POT_STYLE_ID_HARMONIC;
            fprintf(s->flog, "%sPotential between types <%d/%d> (id %d) is set to <%s> (id %d)\n", s->log_msg_prefix, \
                    type1, type2, pot_id, POT_STYLE_NAME_HARMONIC, s->pot_style[pot_id]);
          }
          else if ( !strcmp(args[2], POT_STYLE_NAME_ANHARMONIC) ) {
            s->pot_style[pot_id] = POT_STYLE_ID_ANHARMONIC;
            fprintf(s->flog, "%sPotential between types <%d/%d> (id %d) is set to <%s> (id %d)\n", s->log_msg_prefix, \
                    type1, type2, pot_id, POT_STYLE_NAME_ANHARMONIC, s->pot_style[pot_id]);
          }
          else if ( !strcmp(args[2], POT_STYLE_NAME_LJ) ) {
            s->pot_style[pot_id] = POT_STYLE_ID_LJ;
            fprintf(s->flog, "%sPotential between types <%d/%d> (id %d) is set to <%s> (id %d)\n", s->log_msg_prefix, \
                    type1, type2, pot_id, POT_STYLE_NAME_LJ, s->pot_style[pot_id]);
          }
          else {
            fprintf(stderr, "%s: BAD POT STYLE -- don't recognize potential style <%s>\n", __func__, args[2]);
            return RETURN_ERROR;
          }
        }
        else {
          fprintf(stderr, "%s: BAD POT STYLE -- atom types in pot_style must be ascending\n", __func__);
          return RETURN_ERROR;
        }
      }
      else {
        fprintf(stderr, "%s: BAD ARGS -- pot_style expects %d arg, got %d\n", __func__, num_args_expected, num_args);
        return RETURN_ERROR;
      }
    }

    /**************/
    /* pot_params */
    /**************/
    //   Set potential parameters for each interaction
    else if (!strcmp(key, "pot_params")) {
      if (s->units[0] == '\0') {
        fprintf(stderr, "%s: MUST SET UNITS -- must set sim_units before pot_params\n", __func__);
        return RETURN_ERROR;
      }
      if (num_args < 3) {
        fprintf(stderr, "%s: BAD POT PARAMS -- pot_params needs >=3 args, got %d\n", __func__, num_args);
        return RETURN_ERROR;
      }

      int type1 = strtod(args[0], NULL);
      int type2 = strtod(args[1], NULL);
      if ( type1 <= type2 ) {
        int nt = s->num_types;
        int pot_id = (type1-1)*(2*nt-type1)/2 + type2;
        int pot_style = s->pot_style[pot_id];
        if (pot_style == 0) {
          fprintf(stderr, "%s: MUST SET POT STYLE -- must set pot_style first for types <%d/%d>\n", __func__, type1, type2);
        }

        int expected_args;
        switch (pot_style) {
          case POT_STYLE_ID_HARMONIC: {
            expected_args = 3; // kii, d0, dcut -> kii, d0, dcut2
            double kii = strtof(args[2], NULL); // harmonic force constant, in energy / length^2
            double d0 = strtof(args[3], NULL); // eq distance d0, directly in length units
            double dcut = strtof(args[4], NULL); // cutoff dcut, directly in length units
            
            // In aap units, expect kii input in [eV]/A^2, so convert to [amu*(A/ps)^2] / A^2
            if (!strcmp(s->units, "aap")) {
              kii *= eV2aap;
            }
            
            s->pot_param[pot_id][0] = kii;
            s->pot_param[pot_id][1] = d0;
            s->pot_param[pot_id][2] = dcut * dcut; // store as dcut^2
            
            // Calculate and store energy offset, PE(d=dcut) = 1/2 k dcut^2
            double PE_offset = kii * dcut*dcut / 2.0;
            s->pot_param[pot_id][3] = 0.5*PE_offset; // For convenience, store 0.5*PE(d=dcut)
            
            // Print to log
            fprintf(s->flog, "%sSet harmonic parameters: k(2), equilibrium distance, cutoff radius\n", s->log_msg_prefix);
            break;
          }
          case POT_STYLE_ID_ANHARMONIC: {
            expected_args = 5; // kii, kiii, kiv, d0, dcut
            double kii = strtof(args[2], NULL); // harmonic force constant, in energy / length^2
            double kiii = strtof(args[3], NULL); // 3rd-order force constant, in energy / length^3
            double kiv = strtof(args[4], NULL); // 4th-order force constant, in energy / length^4
            double d0 = strtof(args[5], NULL); // eq distance d0, directly in length units
            double dcut = strtof(args[6], NULL); // cutoff dcut, directly in length units
            
            // In aap units, expect k input in [eV], so convert to [amu*(A/ps)^2]
            if (!strcmp(s->units, "aap")) {
              kii *= eV2aap;
              kiii *= eV2aap;
              kiv *= eV2aap;
            }
            
            s->pot_param[pot_id][0] = kii;
            s->pot_param[pot_id][1] = kiii;
            s->pot_param[pot_id][2] = kiv;
            s->pot_param[pot_id][3] = d0;
            s->pot_param[pot_id][4] = dcut * dcut; // For convenience, store dcut as dcut^2
            
            // Calculate and store energy offset, PE(d=dcut) = 1/2 kii dcut^2 + 1/6 kiii dcut^3 + 1/24 kiv dcut^4
            double PE_offset = kii * dcut*dcut / 2.0 + kiii * dcut*dcut*dcut / 6.0 + kiv * dcut*dcut*dcut*dcut / 24.0;
            s->pot_param[pot_id][5] = 0.5*PE_offset; // For convenience, store 0.5*PE(d=dcut)
            
            // Print to log
            fprintf(s->flog, "%sSet anharmonic parameters: k(2), k(3), equilibrium distance, cutoff radius\n", s->log_msg_prefix);
            break;
          }
          case POT_STYLE_ID_LJ: {
            expected_args = 3; // eps, sig, dcut --> eps, sig6, dcut2
            double eps = strtof(args[2], NULL); // energy scale epsilon, in energy units
            double sig = strtof(args[3], NULL); // length scale sigma, directly in length units
            double dcut = strtof(args[4], NULL); // cutoff dcut, directly in length units
            
            // In aap units, expect eps input in eV, so convert to amu*(A/ps)^2
            if (!strcmp(s->units, "aap")) {
              eps *= eV2aap;
            }
            
            double sig6 = sig*sig*sig * sig*sig*sig;
            double dcut2 = dcut*dcut;
            
            s->pot_param[pot_id][0] = eps;
            s->pot_param[pot_id][1] = sig6; // store as sig^6
            s->pot_param[pot_id][2] = dcut2; // store as dcut^2
            
            // Calculate and store energy offset, PE(d=dcut)
            double sig6_dcut6i = sig6 / (dcut2 * dcut2 * dcut2);
            double PE_offset = 4*eps * sig6_dcut6i * (sig6_dcut6i - 1);
            s->pot_param[pot_id][3] = 0.5*PE_offset; // For convenience, store 0.5*PE(d=dcut)
            

            // Print to log
            fprintf(s->flog, "%sSet lj parameters: epsilon, sigma, cutoffradius \n", s->log_msg_prefix);
            break;
          }
          default: {
            fprintf(stderr, "%s: UNMATCHED POT STYLE -- types <%d/%d> given to pot_params have bad or unset pot_style <%d>\n", __func__, type1, type2, pot_style);
            return RETURN_ERROR;
            break;
          }
        }

        if (num_args-2 != expected_args) { // -2 to account for type1 and type2
          fprintf(stderr, "%s: BAD POT PARAMS -- pot style <%d> expects %d params, got %d\n", __func__, pot_style, expected_args, num_args-2);
          return RETURN_ERROR;
        }

      }
      else {
        fprintf(stderr, "%s: BAD POT PARAMS -- atom types in pot_params must be ascending\n", __func__);
        return RETURN_ERROR;
      }
    }

    /************/
    /* timestep */
    /************/
    //   Set the timestep, in time units set by sim_units
    else if (!strcmp(key, "timestep")) {
      if (s->units[0] == '\0') {
        fprintf(stderr, "%s: MUST SET UNITS -- must set sim_units before timestep\n", __func__);
        return RETURN_ERROR;
      }
      
      num_args_expected = 1;
      
      if (num_args == num_args_expected) {
        s->dt = strtof(args[0], NULL);
        fprintf(s->flog, "%sTimestep set to <%f>\n", s->log_msg_prefix, s->dt);
      }
      else {
        fprintf(stderr, "%s: BAD ARGS -- timestep expects %d arg, got %d\n", __func__, num_args_expected, num_args);
        return RETURN_ERROR;
      }
    }

    /***************/
    /* set_routine */
    /***************/
    //   Set up a routine to execute occasionally during MD; analogous to "fix" in LAMMPS
    else if (!strcmp(key, "set_routine")) {
      if (num_args > 1) {

        // Call routine_setup
        if ( routine_setup(s, args, num_args) == RETURN_ERROR ) {
          fprintf(stderr, "%s: ABORTED ROUTINE SETUP -- during setup of routine <%s>\n", __func__, args[0]);
          return RETURN_ERROR;
        }

      }
      else {
        fprintf(stderr, "%s: BAD ARGS -- not enough args to specify a routine\n", __func__);
        return RETURN_ERROR;
      }
    }

    /*****************/
    /* unset_routine */
    /*****************/
    //   "Delete" a routine
    else if (!strcmp(key, "unset_routine")) {
      num_args_expected = 1;
      if (num_args == num_args_expected) {

        // Set the routine style to 0
        int routine_id = strtol(args[0], NULL, 10);
        s->routine_style[routine_id] = 0;

        // Set the routine parameters to 0 (not really necessary)
        for (int routine_param=0; routine_param<MAX_ROUTINE_PARAMS; routine_param++) {
          s->routine_param[routine_id][routine_param] = 0.0;
        }

        // Success...
        fprintf(s->flog, "%sUnset routine with id %d\n", s->log_msg_prefix, routine_id);
      }
      else {
        fprintf(stderr, "%s: BAD ARGS -- unset_routine expects %d arg, got %d\n", __func__, num_args_expected, num_args);
        return RETURN_ERROR;
      }
    }

    /*************/
    /* run_until */
    /*************/
    //   Run simulation!
    else if (!strcmp(key, "run_until")) {
      num_args_expected = 1;

      // Two different cases:
      //   (1) run until a maximum step (1 arg)
      //   (2) run until thermal steady state (3 args)

      // Grab the step number when the simulation will stop
      if (num_args == 1) {
        s->max_step = strtod(args[0], NULL);
        fprintf(s->flog, "%sRunning MD until step <%d>\n", s->log_msg_prefix, s->max_step);
      }
      else if (num_args == 3 && !strcmp(args[0], "steady")) { // arg 1: keyword "steady"

        // arg 2: id of a temp_profile routine
        int routine_id = strtol(args[1], NULL, 10);
        if (s->routine_style[routine_id] != ROUTINE_ID_TEMP_PROFILE) {
          fprintf(stderr, "%s: BAD ROUTINE -- run_until expects id of a temp_profile routine, got %d\n", __func__, routine_id);
          return RETURN_ERROR;
        } else {
          s->run_until_routine_id = routine_id;
        }

        // arg 3: convergence threshold for bin temps
        s->run_until_conv_T = strtof(args[2], NULL);
        
        // set special value of s->max_step to indicate running until steady state
        s->max_step = MAX_STEP_STEADY;
        fprintf(s->flog, "%sRunning MD until temps in routine <%d> converge within <%.1f%%>\n", s->log_msg_prefix, s->run_until_routine_id, 100*s->run_until_conv_T);
      }
      else {
        fprintf(stderr, "%s: BAD ARGS -- run_until expects 1 or 3 args, got %d\n", __func__, num_args);
        return RETURN_ERROR;
      }

      return RETURN_SUCCESS;
    }

    /*************/
    /* set_step */
    /*************/
    //   Set the current step of the simulation
    else if (!strcmp(key, "set_step")) {
      
      num_args_expected = 1;
      
      if (num_args == num_args_expected) {
        s->step = strtol(args[0], NULL, 10);
        fprintf(s->flog, "%sSimulation step set to <%d>\n", s->log_msg_prefix, s->step);
      }
      else {
        fprintf(stderr, "%s: BAD ARGS -- set_step expects %d arg, got %d\n", __func__, num_args_expected, num_args);
        return RETURN_ERROR;
      }
    }
    
    /****************/
    /* unrecognized */
    /****************/
    //   Not a fatal error, but recorded to the logfile
    else {
      fprintf(s->flog, "%sWARNING -- Skipping unrecognized keyword <%s>\n", s->log_msg_prefix, key);
    }

  } // end while !eof
  
  // If the function gets to this point without finding a <run> command, we're done!
  fprintf(s->flog, "%sJob finished!\n", s->log_msg_prefix);
  return RETURN_END_OF_SCRIPT;
  
} // END READ_SCRIPT



// Aux functions

int tokenize(char *buffer, char key[], char args[][MAX_SCRIPT_ARG_LENGTH]) {
  int num_args = 0;
  char temp_buffer[MAX_LINE_LENGTH];
  char *temp_arg;
  
  // Get keyword and num_args
  strcpy(temp_buffer, buffer); // act on a temporary buffer
  strcpy(key, strtok(temp_buffer, " ")); // get the keyword
  if (key[strlen(key)-1] == '\n') { // strip trailing \n
    key[strlen(key)-1] = '\0';
  }
  do {
    temp_arg = strtok(NULL, " "); // count every following word
    if (temp_arg==NULL || !strcmp(temp_arg,"\n")) {
      continue;
    }
    else {
      num_args++;
    }
  } while (temp_arg);
  
  // Grab settings, put into the array args
  temp_arg = strtok(buffer, " "); // skip the keyword
  for (int i=0; i<num_args; i++) {
    temp_arg = strtok(NULL, " ");
    if (temp_arg[strlen(temp_arg)-1] == '\n') { // strip trailing \n
      temp_arg[strlen(temp_arg)-1] = '\0';
    }
    
    strcpy(args[i], temp_arg);
  }
  
  return num_args;
} // END TOKENIZE




int read_initial_config(Sim *s, char *filename) {
  // See MATLAB script for how the initial config file is currently formatted.
  // For now, just throw this function together. Make it more robust later.
  FILE *finit;
  if ( (finit = fopen(filename, "r")) == NULL ) {
    fprintf(stderr, "%s: BAD INPUT FILE -- cannot open <%s>\n", __func__, filename);
    return RETURN_ERROR;
  }
  
  // Header line
  char trash[MAX_LINE_LENGTH];
  fgets(trash, MAX_LINE_LENGTH, finit);
  
  // Number of atoms
  fscanf(finit, "%d\n", &(s->N));
  if (s->N > MAX_ATOMS) {
    fprintf(stderr, "%s: TOO MANY ATOMS -- N = %d exceeds MAX_ATOMS = %d\n", __func__, s->N, MAX_ATOMS);
    return RETURN_ERROR;
  }
  
  // Data
  int args_read = 0;
  switch (s->sim_dim) {
    case 1:
      for (int i=0; i < (s->N); i++) {
        args_read = fscanf(finit, "%d %d %lf %lf %lf\n", &(s->atom_id[i]), &(s->atom_type[i]), &(s->atom_m[i]), \
                           &(s->atom_r[i][0]), \
                           &(s->atom_v[i][0]));
      }
      break;
    case 2:
      for (int i=0; i < s->N; i++) {
        args_read = fscanf(finit, "%d %d %lf %lf %lf %lf %lf\n", &(s->atom_id[i]), &(s->atom_type[i]), &(s->atom_m[i]), \
                           &(s->atom_r[i][0]), &(s->atom_r[i][1]), \
                           &(s->atom_v[i][0]), &(s->atom_v[i][1]));
      }
      break;
    case 3:
      for (int i=0; i < s->N; i++) {
        args_read = fscanf(finit, "%d %d %lf %lf %lf %lf %lf %lf %lf\n", &(s->atom_id[i]), &(s->atom_type[i]), &(s->atom_m[i]), \
                           &(s->atom_r[i][0]), &(s->atom_r[i][1]), &(s->atom_r[i][2]), \
                           &(s->atom_v[i][0]), &(s->atom_v[i][1]), &(s->atom_v[i][2]));
      }
      break;
    default:
      break;
  }
  if (args_read != 3 + 2*(s->sim_dim)) {
    fprintf(stderr, "%s: BAD INPUT DATA -- error reading <%s>\n", __func__, filename);
    return RETURN_ERROR;
  }
  // take same data for "old" fields too
  for (int i=0; i < (s->N); i++) {
    for (int dim=0; dim < (s->sim_dim); dim++) {
      s->atom_r_old[i][dim] = s->atom_r[i][dim];
      s->atom_v_old[i][dim] = s->atom_v[i][dim];
    }
  }

  // get the number of atom types
  for (int i=0; i < s->N; i++) {
    if (s->atom_type[i] == 0) {
      fprintf(stderr, "%s: ATOM TYPE ZERO -- atom id %d cannot have type <0>\n", __func__, s->atom_id[i]);
      return RETURN_ERROR;
    }
    if (s->atom_type[i] > MAX_ATOM_TYPES) {
      fprintf(stderr, "%s: TOO MANY ATOM TYPES -- type %d of atom %d exceeds MAX_ATOM_TYPES = %d\n", __func__, s->atom_type[i], s->atom_id[i], MAX_ATOM_TYPES);
      return RETURN_ERROR;
    }
    s->num_types = (s->atom_type[i] > s->num_types)? s->atom_type[i] : s->num_types;
  }

  // make sure there aren't any missing atom types
  for (int t=1; t <= s->num_types; t++) { // for every atom TYPE,
    for (int i=0; i < s->N; i++) { // make sure there's at least one atom
      if (s->atom_type[i] == t) {
        break;
      }
      else if (i == (s->N - 1)) {
        fprintf(stderr, "%s: MISSING ATOM TYPE -- no atoms of type <%d>\n", __func__, t);
        return RETURN_ERROR;
      }
    }
  }
  
  return RETURN_SUCCESS;
} // END READ_INITIAL_CONFIG
