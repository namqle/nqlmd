/*
 *  main.c
 *  nqlmd
 *
 *  Created by Nam Quang Le on 11/11/12.
 *  Copyright 2012 Nam Q. Le (University of Virginia). All rights reserved.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "common.h"
#include "read_script.h"
#include "md.h"
#include "cleanup.h"

int main (int argc, const char *argv[]) {
  
  /**********/
  /* PRE-MD */
  /**********/
  
  // Check for 2-part call: <exec> <jobname>
  if (argc!=2) {
    fprintf(stderr, "%s: ABORTED -- bad call; expect \"%s <jobname>\"\n", __func__, argv[0]);
    return RETURN_ERROR;
  }

  // Create a [pointer to a] Sim structure (see common.h)
  Sim *s = malloc(sizeof *s);
  if ( initialize_Sim(s, argv[1]) == RETURN_ERROR ) {
    fprintf(stderr, "%s: ABORTED -- failed to initialize Sim structure\n", __func__);
    return RETURN_ERROR;
  }
  s->time_premd += (clock() - s->start_premd) / CLOCKS_PER_SEC;
  
  /******/
  /* MD */
  /******/
  
  // Loop until we get to the end of the input script...
  while (1) {

    s->start_premd = clock();

    // Read the input script (until a "run" command)
    int outcome = read_script(s);
    if ( outcome == RETURN_ERROR ) {
      fprintf(stderr, "%s: ABORTED -- error reading command script\n", __func__);
      return RETURN_SUCCESS;
    }
    else if ( outcome == RETURN_END_OF_SCRIPT ) {
      break;
    }

    s->time_premd += (clock() - s->start_premd) / CLOCKS_PER_SEC;
    
    // Run a simulation with those settings
    s->start_md = clock();
    if ( md(s) == RETURN_ERROR ) {
      fprintf(stderr, "%s: ABORTED -- error during md\n", __func__);
      return RETURN_ERROR;
    }
    s->time_md += (clock() - s->start_md) / CLOCKS_PER_SEC;
  }
  
  /***********/
  /* POST-MD */
  /***********/
  if ( cleanup(s) == RETURN_ERROR ) {
    fprintf(stderr, "%s: ABORTED -- error during cleanup\n", __func__);
    return RETURN_ERROR;
  }
  
  /* EXIT */
  return RETURN_SUCCESS;
}
