/*
 *  forces.c
 *  nqlmd
 *
 *  Created by Nam Quang Le on 11/26/12.
 *  Copyright 2012 Nam Q. Le (University of Virginia). All rights reserved.
 *
 */

#include <stdio.h>
#include <math.h>
#include <time.h>

#include "common.h"
#include "forces.h"

int forces(Sim *s) {
  
  // Initialize
  s->start_forces = clock();
  s->PE = 0;
  s->virial = 0;

  for (int i=0; i<(s->N); i++) {
    
    // Reset PE to zero
    s->atom_PE[i] = 0;
    
    for (int dim=0; dim < (s->sim_dim); dim++) {  
      // Store current forces as "old forces"
      s->atom_f_old[i][dim] = s->atom_f[i][dim];
      
      // Reset forces to zero
      s->atom_f[i][dim] = 0;
    }
  }
  
  // Accumulate contributions to energy and forces
  int typei, typej, pot_id;
  double d_proj[MAX_DIM]; // projected (componentwise) distance
  double d2; // true distance
  for (int i=0; i < (s->N)-1; i++) { // For each atom i, loop over neighbors
    for (int nb=0; nb < (s->atom_num_nb[i]); nb++) { // (loop over neighbor list, with index "nb")
      int j = s->atom_nblist[i][nb]; // (for more details see build_nblist in routine_run)
      
      // Determine interaction type
      typei = s->atom_type[i];
      typej = s->atom_type[j];
      pot_id = (typei-1)*(2*(s->num_types)-typei)/2 + typej;
      
      // Get distance to nearest image
      d_proj[0] = d_proj[1] = d_proj[2] = 0;
      d2 = 0;
      for (int dim=0; dim < (s->sim_dim); dim++) {
        d_proj[dim] = s->atom_r[j][dim] - s->atom_r[i][dim];
        if (s->sim_pbc[dim]) {
          /* Slow, clever way */
          //d_proj[dim] -= s->sim_box_L[dim] * round(d_proj[dim] / s->sim_box_L[dim]);
          /* Fast, "dumb" way */
          if      (d_proj[dim] >  (s->sim_box_halfL[dim])) {
            d_proj[dim] -= s->sim_box_L[dim];
          }
          else if (d_proj[dim] < -(s->sim_box_halfL[dim])) {
            d_proj[dim] += s->sim_box_L[dim];
          }
        }
        d2 += d_proj[dim]*d_proj[dim];
      }
      
      // Switch according to the type of potential for this pair
      int pot_style_id = s->pot_style[pot_id];
      switch ( pot_style_id ) {
        case POT_STYLE_ID_HARMONIC: {
          double d = sqrt(d2);
          double k = s->pot_param[pot_id][0];
          double d_disp = d - s->pot_param[pot_id][1];
          double dcut2 = s->pot_param[pot_id][2];
          // double offset = s->pot_param[pot_id][3];
          
          // If distance is within cutoff,
          if (d2 < dcut2) {
            // update forces and virial
            double f_abs = k * d_disp / d;
            for (int dim=0; dim < (s->sim_dim); dim++) {
              double f_proj = f_abs * d_proj[dim];
              s->atom_f[i][dim] +=  f_proj;
              s->atom_f[j][dim] += -f_proj;
              s->virial += d_proj[dim] * f_proj;
            }
            // update PE
            // double half_PE = k * d_disp*d_disp / 4.0 - offset;
            double half_PE = k * d_disp*d_disp / 4.0; // no offset for now
            s->atom_PE[i] += half_PE;
            s->atom_PE[j] += half_PE;
            s->PE += 2*half_PE;
          }
          break;
        }
        case POT_STYLE_ID_ANHARMONIC: {
          double d = sqrt(d2);
          double kii = s->pot_param[pot_id][0];
          double kiii = s->pot_param[pot_id][1];
          double kiv = s->pot_param[pot_id][2];
          double d_disp = d - s->pot_param[pot_id][3];
          double dcut2 = s->pot_param[pot_id][4];
          //double offset = s->pot_param[pot_id][5];
          
          // If distance is within cutoff,
          if (d2 < dcut2) {
            // update forces and virial
            double d_disp2 = d_disp*d_disp;
            double f_abs = (kii + kiii*d_disp/2.0 + kiv*d_disp2/6.0)*d_disp / d;
            for (int dim=0; dim < (s->sim_dim); dim++) {
              double f_proj = f_abs * d_proj[dim];
              s->atom_f[i][dim] +=  f_proj;
              s->atom_f[j][dim] += -f_proj;
              s->virial += d_proj[dim] * f_proj;
            }
            // update PE
            //double half_PE = (kii*d_disp2/2.0 + kiii*d_disp2*d_disp/6.0 + kiv*d_disp2*d_disp2/24.0) / 2.0 - offset;
            double half_PE = (kii*d_disp2/2.0 + kiii*d_disp2*d_disp/6.0 + kiv*d_disp2*d_disp2/24.0) / 2.0; // no offset for now
            s->atom_PE[i] += half_PE;
            s->atom_PE[j] += half_PE;
            s->PE += 2*half_PE;
          }
          break;
        }
        case POT_STYLE_ID_LJ: {
          double eps = s->pot_param[pot_id][0];
          double sig6 = s->pot_param[pot_id][1];
          double dcut2 = s->pot_param[pot_id][2];
          double offset = s->pot_param[pot_id][3];

          // If distance is within cutoff,
          if (d2 < dcut2) {
            // precalculate
            double d2i = 1/d2;
            double sig6_d6i = sig6*d2i*d2i*d2i;
            // update forces and virial
            double f_abs = 48*eps * sig6_d6i*(sig6_d6i - 0.5) * d2i;
            for (int dim=0; dim < (s->sim_dim); dim++) {
              double f_proj = f_abs * d_proj[dim];
              s->atom_f[i][dim] += -f_proj;
              s->atom_f[j][dim] +=  f_proj;
              s->virial += d_proj[dim] * f_proj;
            }
            // update PE
            double half_PE = 2*eps * sig6_d6i * (sig6_d6i - 1) - offset;
            s->atom_PE[i] += half_PE;
            s->atom_PE[j] += half_PE;
            s->PE += 2*half_PE;
          }
          break;
        }
        case 0: {
          fprintf(stderr, "%s: UNSET POTENTIAL -- haven't set potential for types <%d/%d>\n", __func__, typei, typej);
          return RETURN_ERROR;
        }
        default:
          fprintf(stderr, "%s: BAD POTENTIAL ID -- don't know how to get forces for types <%d/%d> (pot_id <%d>)\n", __func__, typei, typej, pot_style_id);
          return RETURN_ERROR;
      } // end switch on pot_style_id

    } // inner atom loop
  } // outer atom loop
  
  s->time_forces += (double) (clock() - s->start_forces) / CLOCKS_PER_SEC;
  return RETURN_SUCCESS;
}
