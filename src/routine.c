/*
 *  routine.c
 *  nqlmd
 *
 *  Created by Nam Quang Le on 11/25/12.
 *  Copyright 2012 Nam Q. Le (University of Virginia). All rights reserved.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "common.h"
#include "forces.h"

// Prototypes and constants in routine.h
#include "routine.h"

// Implementations here

/*****************/
/* ROUTINE_SETUP */
/*****************/
extern int routine_setup(Sim *s, char args[][MAX_SCRIPT_ARG_LENGTH], int num_args) {
  
  // Grab the ID of the routine
  int routine_id = strtol(args[0], NULL, 10);
  if (routine_id < 0 || routine_id >= MAX_ROUTINES) {
    fprintf(stderr, "%s: BAD ROUTINE ID -- must be an integer between 0 and %d\n", __func__, MAX_ROUTINES-1);
    return RETURN_ERROR;
  }

  // "Switch" according to the type of routine
  char *routine_name = args[1];

  num_args -= 2;
  int num_args_expected = 0;
  
  /*************/
  /* INTEGRATE */
  /*************/
  if (!strcmp(routine_name, "integrate")) {
    num_args_expected = 2; // two args: <integration method> <group id>

    if (num_args == num_args_expected-1) { // if no group id is given, assume a default of 0 ('all atoms')
      strcpy(args[3], "0");
      num_args = 2;
    }

    if (num_args == num_args_expected) {
      
      // Add the routine
      s->routine_style[routine_id] = ROUTINE_ID_INTEGRATE;
      
      // First arg: integration scheme
      if (!strcmp(args[2], "verlet")) { // BASIC VERLET SCHEME
        
        // Nuance for Verlet: Incorporate v0 through r_old (since basic Verlet doesn't use velocities!)
        s->routine_param[routine_id][0] = INTEGRATE_ID_VERLET;

        if (s->N == 0) { // init_file must have been set
          fprintf(stderr, "%s: NEED INITIAL CONFIG -- must set init_file before Verlet integrator\n", __func__);
          return RETURN_ERROR;
        }
        else if (s->dt == 0) { // need dt
          fprintf(stderr, "%s: NEED TIMESTEP -- must have set timestep before Verlet integrator\n", __func__);
          return RETURN_ERROR;
        }
        else if (s->sim_dim == 0) { // and dimensionality too
          fprintf(stderr, "%s: NEED DIM -- must have set sim_dim before Verlet integrator\n", __func__);
          return RETURN_ERROR;
        }
        else {
          for (int i=0; i < (s->N); i++) {
            for (int dim=0; dim < (s->sim_dim); dim++) {
              s->atom_r_old[i][dim] = s->atom_r[i][dim] - s->dt * s->atom_v[i][dim];
            }
          }
        }
        
      }
      else if (!strcmp(args[2], "vverlet")) { // VELOCITY VERLET SCHEME
        s->routine_param[routine_id][0] = INTEGRATE_ID_VVERLET;
      }
      else { // UNRECOGNIZED SCHEME
        fprintf(stderr, "%s: BAD INTEGRATOR -- unrecognized algorithm <%s>\n", __func__, args[2]);
        return RETURN_ERROR;
      }

      // Second arg: group to integrate
      s->routine_param[routine_id][1] = strtod(args[3], NULL);
      
      // Set the log string
      int group_id = (int)(s->routine_param[routine_id][1] + 0.5);
      if (group_id == 0) {
        fprintf(s->flog, "%sIntegrator set to <%s> (id %d) for <ALL atoms>\n", s->log_msg_prefix, args[2], (int)(s->routine_param[routine_id][0] + 0.5));
      }
      else {
        fprintf(s->flog, "%sIntegrator set to <%s> (id %d) for <atoms in group %d>\n", s->log_msg_prefix, args[2], (int)(s->routine_param[routine_id][0] + 0.5), group_id);
      }
    }
    else {
      fprintf(stderr, "%s: BAD ARGS -- routine <%s> expects %d args, got %d\n", __func__, routine_name, num_args_expected, num_args);
      return RETURN_ERROR;
    }
  }

  /****************/
  /* BUILD_NBLIST */
  /****************/
  else if (!strcmp(routine_name, "build_nblist")) {
    num_args_expected = 2; // two args: <interval> <radius>
    if (num_args == num_args_expected) {
      
      // Add the routine
      s->routine_style[routine_id] = ROUTINE_ID_BUILD_NBLIST;
      
      // First arg: write interval
      s->routine_param[routine_id][0] = strtod(args[2], NULL);
      
      // Second arg: neighbor radius
      s->routine_param[routine_id][1] = strtod(args[3], NULL);
      
      // Set the log string
      fprintf(s->flog, "%sWill build the neighbor list every <%d> steps, with radius <%f>\n", s->log_msg_prefix, (int)(s->routine_param[routine_id][0] + 0.5), s->routine_param[routine_id][1]);
            
    }
    else {
      fprintf(stderr, "%s: BAD ARGS -- routine <%s> expects %d args, got %d\n", __func__, routine_name, num_args_expected, num_args);
      return RETURN_ERROR;
    }
  }
  /************/
  /* BAROSTAT */
  /************/
  else if (!strcmp(routine_name, "barostat")) {
    num_args_expected = 3; // three args: <target pressure> <compressibility> <time constant>
    if (num_args == num_args_expected) {
      
      // Add the routine
      s->routine_style[routine_id] = ROUTINE_ID_BAROSTAT;
      
      // First arg: target pressure
      s->routine_param[routine_id][0] = strtod(args[2], NULL);
      
      // Second arg: compressibility
      s->routine_param[routine_id][1] = strtod(args[3], NULL);
      
      // Third arg: time constant
      s->routine_param[routine_id][2] = strtod(args[4], NULL);
      
      // Set the log string
      fprintf(s->flog, "%sBerendsen barostat set to %f N m^-%d\n", s->log_msg_prefix, s->routine_param[routine_id][0], s->sim_dim-1);
      
    }
    else {
      fprintf(stderr, "%s: BAD ARGS -- routine <%s> expects %d args, got %d\n", __func__, routine_name, num_args_expected, num_args);
      return RETURN_ERROR;
    }
  }
  /***************/
  /* HEAT_FLUX   */
  /***************/
  else if (!strcmp(routine_name, "heat_flux")) {
    num_args_expected = 2; // two args: <group id> <energy/time>
    if (num_args == num_args_expected) {
      
      // Add the routine
      s->routine_style[routine_id] = ROUTINE_ID_HEAT_FLUX;
      
      // First arg: group id
      s->routine_param[routine_id][0] = strtod(args[2], NULL);
      
      // Second arg: energy/time to deposit
      s->routine_param[routine_id][1] = strtod(args[3], NULL);
      
      // Set the log string
      fprintf(s->flog, "%sWill scale atoms in group %d by %f [energy/time]\n", s->log_msg_prefix, (int) s->routine_param[routine_id][0], s->routine_param[routine_id][1]);
            
    }
    else {
      fprintf(stderr, "%s: BAD ARGS -- routine <%s> expects %d args, got %d\n", __func__, routine_name, num_args_expected, num_args);
      return RETURN_ERROR;
    }
  }
  
  /****************/
  /* TEMP_PROFILE */
  /****************/
  else if (!strcmp(routine_name, "temp_profile")) {
    num_args_expected = 4; // 4 args: <dim> <# bins> <sampling increment> <write increment>
    if (num_args == num_args_expected) {
      
      // Add the routine
      s->routine_style[routine_id] = ROUTINE_ID_TEMP_PROFILE;

      // Open the file for writing
      char profile_file_name[MAX_LINE_LENGTH];
      strcpy(profile_file_name, s->jobname);
      strcat(profile_file_name, ".profile");
      if ( (s->fprofile = fopen(profile_file_name, "w")) == NULL ) {
        fprintf(stderr, "%s: BAD PROFILE FILE -- cannot open <%s>\n", __func__, profile_file_name);
        return RETURN_ERROR;
      }
      setlinebuf(s->fprofile);
      
      // First arg: dimension along which the temperature profile is obtained (1, 2, 3 -> x, y, z)
      s->routine_param[routine_id][0] = strtod(args[2], NULL);
      
      if (s->routine_param[routine_id][0] < 1 || s->routine_param[routine_id][0] > s->sim_dim+1) { // make sure the chosen dimension is ok
        fprintf(stderr, "%s: BAD DIMENSION -- must be consistent with sim_dim\n", __func__);
        return RETURN_ERROR;
      }
      
      // Second arg: the number of bins to create
      s->routine_param[routine_id][1] = strtod(args[3], NULL);
      
      // Third arg: compute the averages by sampling every N steps
      s->routine_param[routine_id][2] = strtod(args[4], NULL);
      
      // Fourth arg: write the temperature profile every N steps
      s->routine_param[routine_id][3] = strtod(args[5], NULL);
      
      // Set the log string
      fprintf(s->flog, "%sWill write temperature profile in %d bins every %d steps\n", s->log_msg_prefix, (int) s->routine_param[routine_id][1], (int) s->routine_param[routine_id][3]);
      
    }
    else {
      fprintf(stderr, "%s: BAD ARGS -- routine <%s> expects %d args, got %d\n", __func__, routine_name, num_args_expected, num_args);
      return RETURN_ERROR;
    }
  }
  

  /***************/
  /* WRITE_MACRO */
  /***************/
  else if (!strcmp(routine_name, "write_macro")) {
    num_args_expected = 1; // one arg: <interval>
    if (num_args == num_args_expected) {
      
      // Add the routine
      s->routine_style[routine_id] = ROUTINE_ID_WRITE_MACRO;
      
      // First arg: write interval
      s->routine_param[routine_id][0] = strtod(args[2], NULL);
      
      // Set the log string
      fprintf(s->flog, "%sWill write thermo properties to log file every <%d> steps\n", s->log_msg_prefix, (int)(s->routine_param[routine_id][0] + 0.5));
            
    }
    else {
      fprintf(stderr, "%s: BAD ARGS -- routine <%s> expects %d args, got %d\n", __func__, routine_name, num_args_expected, num_args);
      return RETURN_ERROR;
    }
  }

  /***************/
  /* WRITE_MICRO */
  /***************/
  else if (!strcmp(routine_name, "write_micro")) {
    num_args_expected = 1; // one arg: <interval>
    if (num_args == num_args_expected) {
      
      // Add the routine
      s->routine_style[routine_id] = ROUTINE_ID_WRITE_MICRO;
      
      // First arg: write interval
      s->routine_param[routine_id][0] = strtod(args[2], NULL);
      
      // Hidden variable: whether file has already been started
      s->routine_param[routine_id][1] = 0;
      
      // Set the log string
      char micro_file[MAX_LINE_LENGTH];
      strcpy(micro_file, s->jobname);
      strcat(micro_file, ".micro");
      fprintf(s->flog, "%sWill write atomic data to <%s> every <%d> steps\n", s->log_msg_prefix, micro_file, (int)(s->routine_param[routine_id][0] + 0.5));
            
    }
    else {
      fprintf(stderr, "%s: BAD ARGS -- routine <%s> expects %d args, got %d\n", __func__, routine_name, num_args_expected, num_args);
      return RETURN_ERROR;
    }
  }

  /****************/
  /* WRITE_LAMMPS */
  /****************/
  else if (!strcmp(routine_name, "write_lammps")) {
    num_args_expected = 1; // one arg: <interval>
    if (num_args == num_args_expected) {
      
      // Add the routine
      s->routine_style[routine_id] = ROUTINE_ID_WRITE_LAMMPS;
      
      // First arg: write interval
      s->routine_param[routine_id][0] = strtod(args[2], NULL);
      
      // Hidden variable: whether file has already been started
      s->routine_param[routine_id][1] = 0;
      
      // Set the log string
      char lammps_file[MAX_LINE_LENGTH];
      strcpy(lammps_file, s->jobname);
      strcat(lammps_file, ".lammpstrj");
      fprintf(s->flog, "%sWill write to <%s> every <%d> steps\n", s->log_msg_prefix, lammps_file, (int)(s->routine_param[routine_id][0] + 0.5));
            
    }
    else {
      fprintf(stderr, "%s: BAD ARGS -- routine <%s> expects %d args, got %d\n", __func__, routine_name, num_args_expected, num_args);
      return RETURN_ERROR;
    }
  }
  
  /*************/
  /* WRITE_CFG */
  /*************/
  else if (!strcmp(routine_name, "write_cfg")) {
    num_args_expected = 1; // one arg: <interval>
    if (num_args == num_args_expected) {
      
      // Add the routine
      s->routine_style[routine_id] = ROUTINE_ID_WRITE_CFG;
      
      // First arg: write interval
      s->routine_param[routine_id][0] = strtod(args[2], NULL);
      
      // Hidden variable: whether file has already been started
      s->routine_param[routine_id][1] = 0;
      
      // Set the log string
      char cfg_file[MAX_LINE_LENGTH];
      strcpy(cfg_file, s->jobname);
      strcat(cfg_file, ".cfg");
      fprintf(s->flog, "%sWill write to <%s> every <%d> steps\n", s->log_msg_prefix, cfg_file, (int)(s->routine_param[routine_id][0] + 0.5));
      
    }
    else {
      fprintf(stderr, "%s: BAD ARGS -- routine <%s> expects %d args, got %d\n", __func__, routine_name, num_args_expected, num_args);
      return RETURN_ERROR;
    }
  }

  /*************/
  /* WRITE_NMD */
  /*************/
  else if (!strcmp(routine_name, "write_nmd")) {
    num_args_expected = 2; // two args: <interval> <num modes>
    if (num_args == num_args_expected) {
      
      // Only implemented for 1d right now
      if ( s->sim_dim != 1 ) {
        fprintf(stderr, "%s: SORRY! -- routine <%s> is only implemented in 1D right now :(\n", __func__, routine_name);
      }

      // Add the routine
      s->routine_style[routine_id] = ROUTINE_ID_WRITE_NMD;
      
      // First arg: write interval
      s->routine_param[routine_id][0] = strtod(args[2], NULL);
      
      // Second arg: number of normal modes to sample
      s->routine_param[routine_id][1] = strtod(args[3], NULL);
      
      // Hidden variable: whether file has already been started
      s->routine_param[routine_id][2] = 0;
      
      // Set the log string
      char nmd_file[MAX_LINE_LENGTH];
      strcpy(nmd_file, s->jobname);
      strcat(nmd_file, ".nmd");
      fprintf(s->flog, "%sWill write <%d> normal mode coordinates to <%s> every <%d> steps\n", s->log_msg_prefix, (int)(s->routine_param[routine_id][1] + 0.5), nmd_file, (int)(s->routine_param[routine_id][0] + 0.5));
      
    }
    else {
      fprintf(stderr, "%s: BAD ARGS -- routine <%s> expects %d args, got %d\n", __func__, routine_name, num_args_expected, num_args);
      return RETURN_ERROR;
    }
  }

  /*****************/
  /* WRITE_RESTART */
  /*****************/
  else if (!strcmp(routine_name, "write_restart")) {
    fprintf(s->flog, "%sSorry -- the write_restart routine isn't implemented yet\n", s->log_msg_prefix);
  }

  /****************/
  /* UNRECOGNIZED */
  /****************/
  else {
    fprintf(s->flog, "%sRoutine <%s> is not recognized\n", s->log_msg_prefix, args[1]);
    return RETURN_ERROR;
  }
  
  return RETURN_SUCCESS;
} // end routine_setup



/***************/
/* ROUTINE_RUN */
/***************/
extern int routine_run(Sim *s, int num_routine) {
  
  // Switch based on the ID of the given routine
  // (Every routine should have a constant ROUTINE_ID_<NAME> defined)
  int routine_id = s->routine_style[num_routine];
  switch (routine_id) {
      
    /**************/
    /* NO ROUTINE */
    /**************/
    case 0: {
      break;
    }

    /*************/
    /* INTEGRATE */
    /*************/
    //   Integrate the Newton equations of motion
    case ROUTINE_ID_INTEGRATE: {
      
      // Get the group to update
      int group_id = (int) s->routine_param[num_routine][1] + 0.5;

      // Switch based on the ID of the integrator
      int integrate_id = (int) s->routine_param[num_routine][0] + 0.5;
      switch (integrate_id) {
        
        //
        // VERLET ALGORITHM: See Frenkel & Smit, Sec. 4.2.3
        //
        case INTEGRATE_ID_VERLET: {
          
          double atom_r_new = 0;
          double dt = s->dt;
          s->KE = 0;

          //for (int i=0; i < (s->N); i++) { // update all atoms
          for (int atom_id=0; atom_id< (s->group_num_atoms[group_id]); atom_id++) { // only update atoms in the group

            int i = s->group[group_id][atom_id];
            for (int dim=0; dim < (s->sim_dim); dim++) {
              
              // Get new positions: r(t+dt) = 2*r(t) - r(t-dt) + dt^2 * f(t)/m
              atom_r_new = 2*(s->atom_r[i][dim]) - s->atom_r_old[i][dim] + (dt*dt * s->atom_f[i][dim] / s->atom_m[i]);
              
              // Get new velocities: v(t+dt) = (r(t+dt) - r(t-dt)) / (2*dt)
              s->atom_v_old[i][dim] = s->atom_v[i][dim];
              s->atom_v[i][dim] = (atom_r_new - s->atom_r_old[i][dim]) / (2*dt);
              
              // Get new KE: KE(t+dt) = 1/2 m v(t+dt)^2
              s->KE += 0.5 * s->atom_m[i] * (s->atom_v[i][dim]*s->atom_v[i][dim]);
              
              // Finish updating positions: copy current->old and new->current
              s->atom_r_old[i][dim] = s->atom_r[i][dim];
              s->atom_r[i][dim] = atom_r_new;
              
            }
          }
          
          // Get new forces, f(t+dt) based on the new positions r(t+dt)
          if ( forces(s) == RETURN_ERROR ) {
            fprintf(stderr, "%s: ABORTED FORCE CALC -- error getting forces on step <%d>\n", __func__, s->step);
            return RETURN_ERROR;
          }
          break;
        } // % end verlet algorithm
        
        //
        // VELOCITY VERLET ALGORITHM: See Frenkel & Smit, Sec 4.3.1
        //
        case INTEGRATE_ID_VVERLET: {
          
          // "Part 1"
          double v_partial[s->N][s->sim_dim];
          double dt = s->dt;

          //for (int i=0; i < (s->N); i++) {
          for (int atom_id=0; atom_id< (s->group_num_atoms[group_id]); atom_id++) { // only update atoms in the group

            int i = s->group[group_id][atom_id];
            for (int dim=0; dim < (s->sim_dim); dim++) {
              
              // Get new positions: r(t+dt) = r(t) + v(t)*dt + dt^2 * f(t)/(2*m)
              s->atom_r_old[i][dim] = s->atom_r[i][dim];
              s->atom_r[i][dim] = s->atom_r[i][dim] + s->atom_v[i][dim] * dt + (0.5 * dt*dt * s->atom_f[i][dim] / s->atom_m[i]);
              
              // Get "partial" velocities: v' = v(t) + dt*f(t)/(2*m)
              v_partial[i][dim] = s->atom_v[i][dim] + (0.5 * dt * s->atom_f[i][dim] / s->atom_m[i]);
              
            }
          }
          
          // Interlude: Get new forces, f(t+dt) based on the new positions r(t+dt)
          if ( forces(s) == RETURN_ERROR ) {
            fprintf(stderr, "%s: ABORTED FORCE CALC -- error getting forces on step <%d>\n", __func__, s->step);
            return RETURN_ERROR;
          }
          
          // "Part 2"
          s->KE = 0;

          //for (int i=0; i < (s->N); i++) { // update all atoms
          for (int atom_id=0; atom_id< (s->group_num_atoms[group_id]); atom_id++) { // only update atoms in the group

            int i = s->group[group_id][atom_id];
            for (int dim=0; dim < (s->sim_dim); dim++) {
              
              // Get new velocities: v(t+dt) = v' + dt * f(t+dt) / (2*m)
              s->atom_v_old[i][dim] = s->atom_v[i][dim];
              s->atom_v[i][dim] = v_partial[i][dim] + (0.5 * dt * s->atom_f[i][dim] / s->atom_m[i]);
              
              // Get new KE: KE(t+dt) = 1/2 m v(t+dt)^2
              s->KE += 0.5 * s->atom_m[i] * (s->atom_v[i][dim]*s->atom_v[i][dim]);
              
            }
          }
          break;
        } // % end velocity verlet algorithm
        
        //
        // UNRECOGNIZED INTEGRATION SCHEME
        //
        default: {
          fprintf(stderr, "%s: BAD INTEGRATOR: unrecognized integrate_id <%d>", __func__, integrate_id);
          return RETURN_ERROR;
        }
      }
      break;
    }
      
    /****************/
    /* BUILD_NBLIST */
    /****************/
    //   For each atom i, find all other atoms j>i within a certain radius
    case ROUTINE_ID_BUILD_NBLIST: {
      if ( (s->step) % (int)(s->routine_param[num_routine][0] + 0.5) == 0 ) {

        // Initialize
        s->start_nblist = clock();
        double d_proj[MAX_DIM]; // projected (componentwise) distance
        double d2; // true distance
        double radius = s->routine_param[num_routine][1];

        // Loop over pairs of atoms j>i
        int total_neighbors = 0;
        for (int i=0; i < (s->N)-1; i++) {
          // Each atom starts with 0 neighbors
          s->atom_num_nb[i] = 0;
          for (int j=i+1; j < (s->N); j++) {

            // Get distance to nearest image
            d_proj[0] = d_proj[1] = d_proj[2] = 0;
            d2 = 0;
            for (int dim=0; dim < (s->sim_dim); dim++) {
              d_proj[dim] = s->atom_r[j][dim] - s->atom_r[i][dim];
              if ((s->sim_pbc[dim])) {
                if (d_proj[dim] >  (s->sim_box_halfL[dim])) {
                  d_proj[dim] -= s->sim_box_L[dim];
                }
                if (d_proj[dim] < -(s->sim_box_halfL[dim])) {
                  d_proj[dim] += s->sim_box_L[dim];
                }
              }
              d2 += d_proj[dim]*d_proj[dim];
            }

            // If less than the given radius, add to nblist
            if (d2 < radius*radius) {
              s->atom_nblist[i][s->atom_num_nb[i]] = j;
              s->atom_num_nb[i]++;
              total_neighbors++;
            }

          } // end inner atom loop
        } // end outer atom loop

        // Debrief
        fprintf(s->flog, "%sRebuilt neighbor list: total %d neighbors of %d atoms (%.2f average)\n", s->log_msg_prefix, total_neighbors, s->N, ((double)(total_neighbors) / (double)(s->N)));
        s->time_nblist += (double) (clock() - s->start_nblist) / CLOCKS_PER_SEC;
      }
      break;
    }
      
    /************/
    /* BAROSTAT */
    /************/
    //   Vary the size of the simulation domain to control pressure
    //   See Sec III in Berendsen et al, J Chem Phys 81, 3684 (1984)
    case ROUTINE_ID_BAROSTAT: {
      double P_target        = s->routine_param[num_routine][0];
      double compressibility = s->routine_param[num_routine][1];
      double t_relax         = s->routine_param[num_routine][2];
      
      // Convert units
      if (!strcmp(s->units, "aap")) {
        P_target /= ((s2ps*s2ps * m2a) / (kg2amu) ); // N / m^(sim_dim-1) -> aap
        compressibility *= ((s2ps*s2ps * m2a) / (kg2amu) ); // (N / m^(sim_dim-1))^-1 -> aap^-1
      }
      else if (!strcmp(s->units, "si")) { // legacy code from using bar, now does nothing
        P_target /= 1; // N / m^(sim_dim-1)
        compressibility *= 1; // (N / m^(sim_dim-1))^-1
      }
      
      // Calculate volume, temperature, pressure
      double volume = s->sim_box_L[0];
      for (int dim=1; dim < (s->sim_dim); dim++) {
        volume *= s->sim_box_L[dim];
      }
      double T  = s->KE / (0.5 * s->sim_dim * s->N);
      double P = 0;
      if (!strcmp(s->units, "aap")) {
        T /= kB_aap;
        P = (s->N*kB_aap*T + s->virial/s->sim_dim) / volume;
      }
      else if (!strcmp(s->units, "si")) {
        T /= kB_si;
        P = ( (s->N * kB_si * T) + (s->virial / s->sim_dim) ) / volume;
      }
      
      // Get the scaling factor (see eq 31 in Berendsen J Chem Phys 81 3684 1984)
      double mu = cbrt(1 + compressibility * s->dt / t_relax * (P - P_target));
      
      // Scale atomic positions
      for (int i=0; i < s->N; i++) {
        for (int dim=0; dim < s->sim_dim; dim++) {
          s->atom_r[i][dim] *= mu;
        }
      }

      // Scale the box
      for (int dim=0; dim < s->sim_dim; dim++) {
        s->sim_box[2*dim]     *= mu;
        s->sim_box[2*dim+1]   *= mu;
        s->sim_box_L[dim]     *= mu;
        s->sim_box_halfL[dim] *= mu;
      }

      break;
    }
    
    /*************/
    /* HEAT_FLUX */
    /*************/
    //   Scale the KE of a group of atoms by some [energy / time]
    case ROUTINE_ID_HEAT_FLUX: {
      int group_id = (int) s->routine_param[num_routine][0];
      double E_rate = s->routine_param[num_routine][1];

      if (!strcmp(s->units, "aap")) { // for aap units, need to scale eV/ps -> (amu)(A/ps)^2 / ps
        E_rate *= eV2aap;
      }

      // Convert E_rate from (energy per time) to (energy per timestep)
      E_rate *= s->dt;

      // Follow scaling scheme of Jund & Jullien: PRB 59 13707 (1999)

      // First calculate properties of the whole group
      double m_total = 0;
      double KE_total = 0;
      double v_cm[s->sim_dim];
      for (int dim=0; dim < s->sim_dim; dim++) {
        v_cm[dim] = 0.0;
      }

      for (int i=0; i < s->group_num_atoms[group_id]; i++) {
        int atom = s->group[group_id][i];
        for (int dim=0; dim < s->sim_dim; dim++) {
          // Velocity of the group's center of mass
          v_cm[dim] += s->atom_m[atom] * s->atom_v[atom][dim];
          // Total KE of group
          KE_total  += 0.5 * s->atom_m[atom] * s->atom_v[atom][dim] * s->atom_v[atom][dim];
        }
        // Total mass of group
        m_total += s->atom_m[atom];
      }

      for (int dim=0; dim < s->sim_dim; dim++) {
        v_cm[dim] /= m_total;
      }

      // Then calculate the "relative KE"
      double KE_cm = 0;
      for (int dim=0; dim < s->sim_dim; dim++) {
        KE_cm += v_cm[dim] * v_cm[dim];
      }
      KE_cm *= 0.5*m_total;
      
      double v_scale = sqrt( 1 + E_rate/(KE_total-KE_cm) );

      // Finally, loop back over the atoms in the group and scale their velocities
      for (int i=0; i < s->group_num_atoms[group_id]; i++) {
        int atom = s->group[group_id][i];
        for (int dim=0; dim < s->sim_dim; dim++) {
          // v' = v_cm + alpha*(v - v_cm)
          s->atom_v[atom][dim] = v_cm[dim] + v_scale*(s->atom_v[atom][dim] - v_cm[dim]);
        }
      }
      
      break;
    }
      
    /****************/
    /* TEMP_PROFILE */
    /****************/
    //   Obtain an average temperature profile across the system
    case ROUTINE_ID_TEMP_PROFILE: {
      int profile_dim = (int) (s->routine_param[num_routine][0] - 1);
      const int num_bins = (int) s->routine_param[num_routine][1];
      int N_sample = (int) s->routine_param[num_routine][2];
      int N_write = (int) s->routine_param[num_routine][3];
      
      double bin_width = s->sim_box_L[profile_dim] / ((double) num_bins);
      
      // Sampling
      if ( (s->step) % N_sample == 0 || (s->step) == (s->max_step) ) {
        
        // Initialize temporary counters
        double KE_profile[num_bins];
        int num_atoms[num_bins];
        for (int bin=0; bin < num_bins; bin++) {
          KE_profile[bin] = 0.0;
          num_atoms[bin] = 0;
        }
        
        // Keep track of the number of samples between each "writing"
        s->temp_profile_Nsamples++;
        
        // Loop over atoms: for each...
        for (int i=0; i < (s->N); i++) {
          
          // Determine its bin (make sure we use the image inside the box)
          double pos = s->atom_r[i][profile_dim];
          
          if (pos < s->sim_box[2*profile_dim])
            pos += s->sim_box_L[profile_dim];
          else if (pos > s->sim_box[2*profile_dim+1])
            pos -= s->sim_box_L[profile_dim];
          
          int atom_bin = ((int) (pos / bin_width));
          
          // Add its kinetic energy to that bin's KE
          for (int dim=0; dim < (s->sim_dim); dim++) {
            KE_profile[atom_bin] += 0.5*s->atom_m[i] * (s->atom_v[i][dim]*s->atom_v[i][dim]);;
          }
          
          // Keep track of the number of atoms in each bin
          num_atoms[atom_bin]++;
        }
        
        // Now we have the KE in each bin
        for (int bin=0; bin < num_bins; bin++) {
          // Use the KE PER ATOM to update the running mean and stdev
          double KE_bin = KE_profile[bin] / num_atoms[bin];
          // [new mean] = [old mean] + (KE - [old mean]) / [# samples]
          double new_KE_mean = s->temp_profile_KE_mean[bin] + (KE_bin - s->temp_profile_KE_mean[bin]) / s->temp_profile_Nsamples;
          // [new std] = [old std] + (KE - [old mean]) * (KE - [new mean])
          s->temp_profile_KE_var[bin] += (KE_bin - s->temp_profile_KE_mean[bin]) * (KE_bin - new_KE_mean);
          
          s->temp_profile_KE_mean[bin] = new_KE_mean;
        }
      }

      // Do stuff on intervals of N_write
      if ( (s->step)==(s->max_step) || (s->step)%N_write==0 ) {
        // If still approaching steady state, check for convergence of temperatures
        if ( s->max_step == MAX_STEP_STEADY ) {
          // Loop over bins and compare (T,sT) with conv thresholds
          int routine_id = s->run_until_routine_id;        // id of the temp_profile routine
          int num_bins = s->routine_param[routine_id][1];  // number of bins in the profile
          int steady_state = 1;                            // boolean: has steady state been reached?

          for (int bin = 0; bin < num_bins; bin++) {       // for each bin,
            double change = fabs( ( (s->temp_profile_KE_mean[bin]) - (s->temp_profile_KE_mean_old[bin]) ) / (s->temp_profile_KE_mean_old[bin]) );
            //fprintf(s->flog, "%sdT=%f in bin %d\n", s->log_msg_prefix, change, bin);
            if (change >= s->run_until_conv_T) {           // compare change in T with threshold
              steady_state = 0;                            // stop checking at any "violation"
              break;
            }
          }
          if (steady_state) {
            return RETURN_STEADY_STATE;
          }
        }
        // Otherwise, write temperature data to s->fprofile
        else {
          for (int bin=0; bin < num_bins; bin++) {
            // T(bin) = KE(bin) / (0.5 * d * kB)
            double T_mean = s->temp_profile_KE_mean[bin] / (0.5 * s->sim_dim );
            double T_std = 0.0;
            if (s->temp_profile_Nsamples > 1) {
              T_std = sqrt(s->temp_profile_KE_var[bin] / (s->temp_profile_Nsamples - 1)) / (0.5 * s->sim_dim );
            }
            
            if (!strcmp(s->units, "aap")) {
              T_mean  /= kB_aap;
              T_std  /= kB_aap;
            }
            else if (!strcmp(s->units, "si")) {
              T_mean /= kB_si;
              T_std /= kB_si;
            }
            fprintf(s->fprofile, "%3d %.6g %.6g\n", bin, T_mean, T_std);

            // Reset things to start sampling over again
            s->temp_profile_KE_mean[bin] = 0;
            s->temp_profile_KE_var[bin] = 0;
          }
          s->temp_profile_Nsamples = 0;
        }

        // Save current profile as old profile
        for (int bin = 0; bin < num_bins; bin++) {
          s->temp_profile_KE_mean_old[bin] = s->temp_profile_KE_mean[bin];
          s->temp_profile_KE_var_old[bin] = s->temp_profile_KE_var[bin];
        }
      }

      break;
    }

    /***************/
    /* WRITE_MACRO */
    /***************/
    //   Write macroscopic (i.e., thermodynamic) data to the log file
    case ROUTINE_ID_WRITE_MACRO: {
      if (s->step == 0) {
        fprintf(s->flog, "#   Step          T          P            V           KE           PE            E\n");
      }
      if ( (s->step) % (int)(s->routine_param[num_routine][0] + 0.5) == 0 || (s->step) == (s->max_step) ) {
        double PE = s->PE;
        double KE = s->KE;
        double T  = KE / (0.5 * s->sim_dim * s->N);

        double P = 0;
        double P_kinetic = 0;
        double P_virial = 0;
        double volume = s->sim_box_L[0];
        for (int dim=1; dim < (s->sim_dim); dim++) {
          volume *= s->sim_box_L[dim];
        }
        
        // TEST: correction to the LJ pressure; see Frenkel & Smit eq. 3.2.8
        double P_correction = 0; /*
        if (s->pot_style[1] == POT_STYLE_ID_LJ) {
          double rho = s->N / volume;
          double eps = s->pot_param[1][0];
          double sig6 = s->pot_param[1][1];
          double dcut3i = 1 / (s->pot_param[1][2] * sqrt(s->pot_param[1][2]));
          P_correction = 16.0/3.0 * M_PI * rho*rho * eps * sig6*dcut3i * (2.0/3.0 * sig6*dcut3i*dcut3i - 1);
        } */
        
        if (!strcmp(s->units, "aap")) { // for aap units, need to scale (amu)(A/ps)^2 -> eV
          PE /= eV2aap;
          KE /= eV2aap;
          T  /= kB_aap;
          
          P_kinetic = (s->N * kB_aap * T) / volume;
          P_virial = (s->virial / s->sim_dim) / volume;
          
          P = P_kinetic + P_virial + P_correction;
          P *= (s2ps*s2ps * m2a) / (kg2amu); // aap -> SI: N / m^(sim_dim-1)
        }
        else if (!strcmp(s->units, "si")) {
          T /= kB_si;
          
          P = ( (s->N * kB_si * T) + (s->virial / s->sim_dim) ) / volume; // N / m^(sim_dim-1)
        }

        fprintf(s->flog, "%8d %10.4g %10.4g %12.6g %12.6g %12.6g %12.6g\n", s->step, T, P, volume, KE, PE, PE+KE);
      }
      break;
    }
      
    /***************/
    /* WRITE_MICRO */
    /***************/
    //   Write microscopic (i.e., atomic) data to <jobname>.micro
    case ROUTINE_ID_WRITE_MICRO: {
      if ( ((s->step) % (int)(s->routine_param[num_routine][0] + 0.5) == 0) || ((s->step) == (s->max_step)) ) {
        // Open micro file
        FILE *fmicro;
        char micro_file[MAX_LINE_LENGTH];
        strcpy(micro_file, s->jobname);
        strcat(micro_file, ".micro");
        
        if ( (int) s->routine_param[num_routine][1] == 0 ) { // If first call, start a new file
          if ( (fmicro = fopen(micro_file, "w")) == NULL ) {
            fprintf(stderr, "%s: BAD MICRO FILE -- cannot open <%s>\n", __func__, micro_file);
            return RETURN_ERROR;
          }
          fprintf(fmicro, "%% Micro output for jobname <%s>\n", s->jobname);
          /*
          fprintf(fmicro, "%% 1    2    3    4     5  6  7    8  9  10   11 12 13\n");
          fprintf(fmicro, "%% id   type mass PE    x  y  z    vx vy vz   fx fy fz\n");
          */
          /*
          fprintf(fmicro, "%% 1    2    3    4     5  6  7    8  9  10\n");
          fprintf(fmicro, "%% id   type mass PE    x  y  z    vx vy vz\n");
          */
          fprintf(fmicro, "%% 1    2    3    4  5  6 7  8   9\n");
          fprintf(fmicro, "%% id   type mass x  y  z vx vy vz\n");

          s->routine_param[num_routine][1] = 1;
        }
        else {
          if ( (fmicro = fopen(micro_file, "a")) == NULL ) { // Afterwards, append to existing
            fprintf(stderr, "%s: BAD MICRO FILE -- cannot open <%s>\n", __func__, micro_file);
            return RETURN_ERROR;
          }
        }
        // Append the current configuration
        fprintf(fmicro, "%% TIMESTEP %d\n", s->step);
        for (int i=0; i < (s->N); i++) {
          double PE = s->atom_PE[i];
          if (!strcmp(s->units, "aap")) {
            PE /= eV2aap;
          }
          fprintf(fmicro, "%d %d %f %f %f %f %f %f %f\n",
                  s->atom_id[i], s->atom_type[i], s->atom_m[i], /*PE,*/ \
                  s->atom_r[i][0], s->atom_r[i][1], s->atom_r[i][2], \
                  s->atom_v[i][0], s->atom_v[i][1], s->atom_v[i][2]); /*, \
                  s->atom_f[i][0], s->atom_f[i][1], s->atom_f[i][2]); */
        }
        fclose(fmicro);
      }
      break;
    }
    
    /****************/
    /* WRITE_LAMMPS */
    /****************/
    //   Write file in lammps dump format (readable by VMD)
    case ROUTINE_ID_WRITE_LAMMPS: {
      if ( ((s->step) % (int)(s->routine_param[num_routine][0] + 0.5) == 0) || ((s->step) == (s->max_step)) ) {
        // Open lammps file
        FILE *flammps;
        char lammps_file[MAX_LINE_LENGTH];
        strcpy(lammps_file, s->jobname);
        strcat(lammps_file, ".lammpstrj");
        
        if ( (int) s->routine_param[num_routine][1] == 0 ) { // If this is the first call, start a new file
          if ( (flammps = fopen(lammps_file, "w")) == NULL ) {
            fprintf(stderr, "%s: BAD OUTPUT FILE -- cannot open <%s>\n", __func__, lammps_file);
            return RETURN_ERROR;
          }
          s->routine_param[num_routine][1] = 1;
        }
        else {
          if ( (flammps = fopen(lammps_file, "a")) == NULL ) { // Afterwards, append to existing file
            fprintf(stderr, "%s: BAD OUTPUT FILE -- cannot open <%s>\n", __func__, lammps_file);
            return RETURN_ERROR;
          }
        }
        // Append the current configuration
        fprintf(flammps, "ITEM: TIMESTEP\n%d\n", s->step);
        fprintf(flammps, "ITEM: NUMBER OF ATOMS\n%d\n", s->N);
        fprintf(flammps, "ITEM: BOX BOUNDS pp pp pp\n%f %f\n%f %f\n%f %f\n", \
                s->sim_box[0], s->sim_box[1], s->sim_box[2], s->sim_box[3], s->sim_box[4], s->sim_box[5]);
        fprintf(flammps, "ITEM: ATOMS id type x y z vx vy vz\n");
        for (int i=0; i < (s->N); i++) {
          fprintf(flammps, "%d %d %f %f %f %f %f %f\n", s->atom_id[i], s->atom_type[i], \
                  s->atom_r[i][0], s->atom_r[i][1], s->atom_r[i][2], \
                  s->atom_v[i][0], s->atom_v[i][1], s->atom_v[i][2]);
        }
        
        fclose(flammps);
      }
      break;
    }
      
    /*************/
    /* WRITE_CFG */
    /*************/
    //   Write files in cfg format (readable by atomeye) -- separate file for each snapshot
    case ROUTINE_ID_WRITE_CFG: {
      if ( ((s->step) % (int)(s->routine_param[num_routine][0] + 0.5) == 0) || ((s->step) == (s->max_step)) ) {
        // Open cfg file
        FILE *fcfg;
        char cfg_file[MAX_LINE_LENGTH];
        sprintf(cfg_file, "%08d.cfg", s->step);
        
        // Open the file
        if ( (fcfg = fopen(cfg_file, "w")) == NULL ) {
          fprintf(stderr, "%s: BAD OUTPUT FILE -- cannot open <%s>\n", __func__, cfg_file);
          return RETURN_ERROR;
        }
        s->routine_param[num_routine][1] = 1;
        
        // System-wide information
        fprintf(fcfg, "Number of particles = %d\n", s->N);
        fprintf(fcfg, "# (required) this must be the first line\n\n");
        
        if (!strcmp(s->units, "aap")) {
          fprintf(fcfg, "A = 1.0 Angstrom (basic length-scale)\n");
        }
        else if (!strcmp(s->units, "si")) {
          fprintf(fcfg, "A = 1.0 meter (basic length-scale)\n");
        }
        fprintf(fcfg, "# (optional) basic length-scale: default A = 1.0 [Angstrom]\n\n");
        
        double sim_box_L[MAX_DIM];
        for (int dim=0; dim < MAX_DIM; dim++) {
          sim_box_L[dim] = (fabs(s->sim_box_L[dim]) <= 1e-5) ? 10.0 : s->sim_box_L[dim];
        }

        fprintf(fcfg, "H0(1,1) = %f A\n", sim_box_L[0]);
        fprintf(fcfg, "H0(1,2) = 0 A\n");
        fprintf(fcfg, "H0(1,3) = 0 A\n");
        
        fprintf(fcfg, "H0(2,1) = 0 A\n");
        fprintf(fcfg, "H0(2,2) = %f A\n", sim_box_L[1]);
        fprintf(fcfg, "H0(2,3) = 0 A\n");
        
        fprintf(fcfg, "H0(3,1) = 0 A\n");
        fprintf(fcfg, "H0(3,2) = 0 A\n");
        fprintf(fcfg, "H0(3,3) = %f A\n", sim_box_L[2]);
        
        fprintf(fcfg, "# (required) supercell edges, in A\n\n");
        
        fprintf(fcfg, "# ENSUING ARE THE ATOMS, EACH ATOM DESCRIBED BY A ROW\n");
        fprintf(fcfg, "# 1st entry is the atomic mass in amu\n");
        fprintf(fcfg, "# 2nd entry is the chemical symbol (max 2 chars)\n");
        fprintf(fcfg, "# 3rd entry is reduced coordinate s1 (dimensionless)\n");
        fprintf(fcfg, "# 4th entry is reduced coordinate s2 (dimensionless)\n");
        fprintf(fcfg, "# 5th entry is reduced coordinate s3 (dimensionless)\n");
        fprintf(fcfg, "# 6th entry is d(s1)/dt in basic rate-scale R\n");
        fprintf(fcfg, "# 7th entry is d(s2)/dt in basic rate-scale R\n");
        fprintf(fcfg, "# 8th entry is d(s3)/dt in basic rate-scale R\n\n");
        
        fprintf(fcfg, "R = 1.0 [ps^-1]\n");
        fprintf(fcfg, "# (optional) basic rate-scale: default R = 1.0 [ns^-1]\n\n");
        
        // List of atomic data
        for (int i=0; i < (s->N); i++) {
          fprintf(fcfg, "%f %s %f %f %f %f %f %f\n", \
                  s->atom_m[i], \
                  "Ar", \
                  s->atom_r[i][0] / sim_box_L[0], \
                  s->atom_r[i][1] / sim_box_L[1], \
                  s->atom_r[i][2] / sim_box_L[2], \
                  s->atom_v[i][0] / sim_box_L[0], \
                  s->atom_v[i][1] / sim_box_L[1], \
                  s->atom_v[i][2] / sim_box_L[2]);
        }
        
        fclose(fcfg);
      }
      break;
    }
      
    /*************/
    /* WRITE_NMD */
    /*************/
    //   Write normal mode coordinates to a file
    case ROUTINE_ID_WRITE_NMD: {
      if ( ((s->step) % (int)(s->routine_param[num_routine][0] + 0.5) == 0) || ((s->step) == (s->max_step)) ) {

        // Open nmd file
        FILE *fnmd;
        char nmd_file[MAX_LINE_LENGTH];
        strcpy(nmd_file, s->jobname);
        strcat(nmd_file, ".nmd");
        
        if ( (int) s->routine_param[num_routine][2] == 0 ) { // If first call, start a new file
          if ( (fnmd = fopen(nmd_file, "w")) == NULL ) {
            fprintf(stderr, "%s: BAD NMD FILE -- cannot open <%s>\n", __func__, nmd_file);
            return RETURN_ERROR;
          }
          fprintf(fnmd, "%% Normal mode coordinates for jobname <%s>\n", s->jobname);
          fprintf(fnmd, "%% 1                2        3\n");
          fprintf(fnmd, "%% (2pi/wavelength) Re[Qdot] Im[Qdot]\n");

          s->routine_param[num_routine][2] = 1;
        }
        else {
          if ( (fnmd = fopen(nmd_file, "a")) == NULL ) { // Afterwards, append to existing
            fprintf(stderr, "%s: BAD NMD FILE -- cannot open <%s>\n", __func__, nmd_file);
            return RETURN_ERROR;
          }
        }

        // Calculate the wavenumbers q
        double a = s->sim_box_L[0] / s->N;
        int Nq = s->routine_param[num_routine][1];
        double q[Nq];
        for (int iq=0; iq<Nq; iq++) {
          q[iq] = (iq+1.0)/Nq * M_PI/a;
        }

        // Calculate the equilibrium positions r_eq
        double r_eq[s->N];
        for (int i=0; i<(s->N); i++) {
          r_eq[i] = (0.5 + i)*a;
        }

        // Calculate the normal mode coordinate Qdot for each value of q
        fprintf(fnmd, "%% TIMESTEP %d\n", s->step);

        double Qdot_re[Nq];
        double Qdot_im[Nq];
        for (int iq=0; iq<Nq; iq++) {

          Qdot_re[iq] = 0;
          Qdot_im[iq] = 0;

          for (int i=0; i<(s->N); i++) {
            Qdot_re[iq] += sqrt(s->atom_m[i]) * cos(q[iq]*r_eq[i]) * s->atom_v[i][0];
            Qdot_im[iq] -= sqrt(s->atom_m[i]) * sin(q[iq]*r_eq[i]) * s->atom_v[i][0];
          }

          Qdot_re[iq] /= sqrt(s->N);
          Qdot_im[iq] /= sqrt(s->N);

          if (!strcmp(s->units, "aap")) {
            Qdot_re[iq] /= sqrt(eV2aap);
            Qdot_im[iq] /= sqrt(eV2aap);
          }

          // Write to file
          fprintf(fnmd, "%.6g %.6g %.6g\n", q[iq], Qdot_re[iq], Qdot_im[iq]);

        }
        fclose(fnmd);
      }
      break;
    }
      
    /****************/
    /* UNRECOGNIZED */
    /****************/
    default: {
      fprintf(stderr, "%s: BAD ROUTINE ID -- don't know how to execute routine_id <%d>\n", __func__, routine_id);
      return RETURN_ERROR;
    }
  }
  return RETURN_SUCCESS;
} // end routine_run
