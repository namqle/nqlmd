/*
 *  cleanup.c
 *  nqlmd
 *
 *  Created by Nam Quang Le on 11/14/12.
 *  Copyright 2012 Nam Q. Le (University of Virginia). All rights reserved.
 *
 */

#include <stdio.h>
#include <string.h>

#include "cleanup.h"

int cleanup(Sim *s) {
  // Print closing remarks to the log file
  fprintf(s->flog, "\n%sEnd: jobname <%s>\n", s->log_msg_prefix, s->jobname);

  double time_total = s->time_premd + s->time_md;
  s->time_wall += difftime(time(NULL), s->start_wall);

  fprintf(s->flog, "%s   Init time: %8.0f sec, %3.0f%%\n", s->log_msg_prefix, s->time_premd, s->time_premd / time_total * 100);
  fprintf(s->flog, "%s     MD time: %8.0f sec, %3.0f%%\n", s->log_msg_prefix, s->time_md, s->time_md / time_total * 100);
  fprintf(s->flog, "%s     (forces: %8.0f sec, %3.0f%%)\n", s->log_msg_prefix, s->time_forces, s->time_forces / time_total * 100);
  fprintf(s->flog, "%s     (nblist: %8.0f sec, %3.0f%%)\n", s->log_msg_prefix, s->time_nblist, s->time_nblist / time_total * 100);
  fprintf(s->flog, "%s-------------------------------------\n", s->log_msg_prefix);
  fprintf(s->flog, "%sTotal (CPU) : %8.0f sec, 100%%\n", s->log_msg_prefix, time_total);
  fprintf(s->flog, "%sTotal (wall): %8.0f sec\n", s->log_msg_prefix, s->time_wall);
  fprintf(s->flog, "%s-------------------------------------\n", s->log_msg_prefix);
  fprintf(s->flog, "%sPerformance : %8.2f M atom-steps/s\n", s->log_msg_prefix, (double)(s->N * s->total_steps) / time_total * 1e-6);

  fclose(s->fscript);
  fclose(s->flog);

  return RETURN_SUCCESS;
}
