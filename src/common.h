/*
 *  common.h
 *  nqlmd
 *
 *  Created by Nam Quang Le on 11/12/12.
 *  Copyright 2012 Nam Q. Le (University of Virginia). All rights reserved.
 *
 */

// INCLUDE GUARDS
#ifndef COMMON_H
#define COMMON_H

#include <time.h>

// PHYSICAL CONSTANTS
#define kg2amu 6.022141e26 // mass
#define m2a 1e10 // length
#define s2ps 1e12 // time

#define eV2aap 9.6486e3 // energy (aap = "amu Angstrom ps" system)
#define eV2J 1.60217646e-19

#define kB_aap 0.83148 // (amu*(A/ps)^2)/K
#define kB_eV 8.6173e-5 // eV/K
#define kB_si 1.3806503e-23 // J/K

// RETURN VALUES
#define RETURN_SUCCESS       0
#define RETURN_ERROR         1
#define RETURN_END_OF_SCRIPT 2
#define RETURN_STEADY_STATE  3 // used for run_until steady

// PROGRAM CONSTANTS
#define MAX_DIM 3
#define MAX_ATOMS 1000000
#define MAX_NEIGH 100
#define MAX_ATOM_TYPES 5
#define MAX_POT_PARAMS 100
#define MAX_ROUTINES 100
#define MAX_ROUTINE_PARAMS 20

#define MAX_GROUPS 1000
#define MAX_ATOMS_PER_GROUP 100000

#define MAX_LINE_LENGTH 200
#define MAX_SCRIPT_ARGS 100
#define MAX_SCRIPT_ARG_LENGTH 100

#define POT_STYLE_NAME_HARMONIC "harmonic"
#define POT_STYLE_NAME_ANHARMONIC "anharmonic"
#define POT_STYLE_NAME_LJ "lj"
#define POT_STYLE_ID_HARMONIC 1
#define POT_STYLE_ID_ANHARMONIC 2
#define POT_STYLE_ID_LJ 3

#define MAX_TEMP_PROFILE_BINS 1000
#define MAX_STEP_STEADY -1 // store as s->max_step to run until steady state

// Sim: structure that describes a current system state
typedef struct Sim_def Sim;
struct Sim_def {
  
  /* job info */
  char jobname[MAX_LINE_LENGTH];
  char units[MAX_SCRIPT_ARG_LENGTH];
  char log_echo_prefix[MAX_SCRIPT_ARG_LENGTH];
  char log_msg_prefix[MAX_SCRIPT_ARG_LENGTH];

  /* various "stopwatches" */
  clock_t start_wall;
  clock_t start_premd;
  clock_t start_md;
  clock_t start_forces;
  clock_t start_nblist;
  double time_wall;
  double time_premd;
  double time_md;
  double time_forces;
  double time_nblist;
  

  /* file pointers */
  FILE *fscript;
  FILE *flog;
  
  /* system info */
  int N;           // total number of atoms
  int num_types;   // number of types of atoms
  int step;        // current timestep
  int total_steps; // total steps (ignoring jumps due to set_step)
  int max_step;    // timestep to stop AFTER
  double dt;       // time increment
  double PE;       // potential energy
  double KE;       // kinetic energy
  double virial;   // TIME AVERAGE of the virial, sum(f_ij dot r_ij)
  
  /* atom info */
  int atom_id[MAX_ATOMS];    // unique atom id
  int atom_type[MAX_ATOMS];  // atom type
  
  double atom_m[MAX_ATOMS];  // atom masses
  double atom_PE[MAX_ATOMS]; // atom potential energy
  
  double atom_r[MAX_ATOMS][MAX_DIM];     // atom positions
  double atom_v[MAX_ATOMS][MAX_DIM];     // atom velocities
  double atom_f[MAX_ATOMS][MAX_DIM];     // atom forces
  double atom_r_old[MAX_ATOMS][MAX_DIM]; // old atom positions
  double atom_v_old[MAX_ATOMS][MAX_DIM]; // old atom velocities
  double atom_f_old[MAX_ATOMS][MAX_DIM]; // old atom forces
  
  int atom_num_nb[MAX_ATOMS];            // for each atom: number of neighbors
  int atom_nblist[MAX_ATOMS][MAX_NEIGH]; // for each atom: ids of neighbors
  
  /* potential info */
  int pot_style[(MAX_ATOM_TYPES*(MAX_ATOM_TYPES+1))/2]; // for each atom: interaction types
  double pot_param[(MAX_ATOM_TYPES*(MAX_ATOM_TYPES+1))/2][MAX_POT_PARAMS]; // (i,j)=(interaction id, interaction vars); see potential files for how vars are stored
  
  /* simulation domain info */
  int sim_dim;                   // # dimensions in the simulation
  int sim_pbc[MAX_DIM];          // whether each dimension is periodic
  double sim_box[2*MAX_DIM];     // bounds of the simulation box
  double sim_box_L[MAX_DIM];     // lengths of the simulation box
  double sim_box_halfL[MAX_DIM]; // half lengths (for pbc)
  
  /* group info */
  int group_num_atoms[MAX_GROUPS];
  int group[MAX_GROUPS][MAX_ATOMS_PER_GROUP];
  
  /* routine info */
  int routine_style[MAX_ROUTINES];
  double routine_param[MAX_ROUTINES][MAX_ROUTINE_PARAMS];
  
  /* routine temp_profile */
  FILE *fprofile;
  int    temp_profile_Nsamples;
  double temp_profile_KE_mean[MAX_GROUPS];
  double temp_profile_KE_var[MAX_GROUPS];
  double temp_profile_KE_mean_old[MAX_GROUPS];
  double temp_profile_KE_var_old[MAX_GROUPS];

  /* settings for running until thermal steady state */
  int run_until_routine_id; // id of temp_profile to use
  double run_until_conv_T;     // convergence threshold for bin temperatures
  double run_until_conv_sT;    // convergence threshold for bin temp std devs
};

#endif /* COMMON_H */



// Prototype for function that zeros out a Sim structure
extern int initialize_Sim(Sim *s, const char *jobname);
