/*
 *  common.c
 *  nqlmd
 *
 *  Created by Nam Quang Le on 11/26/12.
 *  Copyright 2012 Nam Q. Le (University of Virginia). All rights reserved.
 *
 */

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "common.h"

extern int initialize_Sim(Sim *s, const char *jobname) {
  
  /* job info */
  strcpy(s->jobname, jobname);
  s->units[0] = '\0';
  strcpy(s->log_echo_prefix, "#- ");
  strcpy(s->log_msg_prefix, "#> ");

  /* various "stopwatches" */
  s->start_wall     =  time(NULL);
  s->start_premd    =  clock();
  s->start_md       =  clock();
  s->start_forces   =  clock();
  s->start_nblist   =  clock();
  s->time_wall      =  0;
  s->time_premd     =  0;
  s->time_md        =  0;
  s->time_forces    =  0;
  s->time_nblist    =  0;

  /* file pointers */
  char script_file_name[MAX_LINE_LENGTH];
  strcpy(script_file_name, s->jobname);
  strcat(script_file_name, ".script");
  if ( (s->fscript = fopen(script_file_name, "r")) == NULL ) {
    fprintf(stderr, "%s: BAD SCRIPT FILE -- cannot open <%s>\n", __func__, script_file_name);
    return RETURN_ERROR;
  }
  setlinebuf(s->fscript);

  char log_file_name[MAX_LINE_LENGTH];
  strcpy(log_file_name, s->jobname);
  strcat(log_file_name, ".log");
  if ( (s->flog = fopen(log_file_name, "w")) == NULL ) {
    fprintf(stderr, "%s: BAD LOG FILE -- cannot open <%s>\n", __func__, log_file_name);
    return RETURN_ERROR;
  }
  setlinebuf(s->flog);

  /* system info */
  s->N = 0;
  s->num_types = 0;
  s->step = 0;
  s->total_steps= 0;
  s->max_step = 0;
  s->dt = 0.0;
  s->PE = 0.0;
  s->KE = 0.0;
  
  /* atom info */
  for (int i=0; i<MAX_ATOMS; i++) {
    s->atom_id[i] = 0;
    s->atom_type[i] = 0;
    
    s->atom_m[i] = 0.0;
    s->atom_PE[i] = 0.0;
    
    for (int dim=0; dim<MAX_DIM; dim++) {
      s->atom_r[i][dim] = 0.0;
      s->atom_v[i][dim] = 0.0;
      s->atom_f[i][dim] = 0.0;
      s->atom_r_old[i][dim] = 0.0;
      s->atom_v_old[i][dim] = 0.0;
    }
    
    s->atom_num_nb[i] = 0;
    for (int nb=0; nb<MAX_NEIGH; nb++) {
      s->atom_nblist[i][nb] = 0;
    }
  }
  
  /* potential info */
  for (int pot_id=0; pot_id < ((MAX_ATOM_TYPES*(MAX_ATOM_TYPES+1))/2); pot_id++) {
    s->pot_style[pot_id] = 0;
    for (int pot_param=0; pot_param<MAX_POT_PARAMS; pot_param++) {
      s->pot_param[pot_id][pot_param] = 0.0;
    }
  }
  
  /* simulation domain info */
  s->sim_dim = 0;
  for (int dim=0; dim<MAX_DIM; dim++) {
    s->sim_pbc[dim] = 0;
    s->sim_box[dim] = s->sim_box[dim+1] = 0.0;
    s->sim_box_L[dim] = 0.0;
  }
  
  /* group info */
  for (int group_id=0; group_id<MAX_GROUPS; group_id++) {
    s->group_num_atoms[group_id] = 0;
    for (int i=0; i<MAX_ATOMS_PER_GROUP; i++) {
      s->group[group_id][i] = 0;
    }
  }
  
  /* routine info */
  for (int routine_id=0; routine_id<MAX_ROUTINES; routine_id++) {
    s->routine_style[routine_id] = 0;
    for (int routine_param=0; routine_param<MAX_ROUTINE_PARAMS; routine_param++) {
      s->routine_param[routine_id][routine_param] = 0.0;
    }
  }
  
  /* temperature profile */
  s->temp_profile_Nsamples = 0;
  for (int bin=0; bin<MAX_TEMP_PROFILE_BINS; bin++) {
    s->temp_profile_KE_mean[bin] = 0.0;
    s->temp_profile_KE_var[bin] = 0.0;
    s->temp_profile_KE_mean_old[bin] = 999.9; // initialize the old values to different numbers
    s->temp_profile_KE_var_old[bin] = 999.9;  // to avoid appearance of steady state
  }
  s->run_until_routine_id = 0;
  s->run_until_conv_T = 0.0;
  s->run_until_conv_sT = 0.0;
  
  return RETURN_SUCCESS;
}
