/*
 *  read_script.h
 *  nqlmd
 *
 *  Created by Nam Quang Le on 11/14/12.
 *  Copyright 2012 Nam Q. Le (University of Virginia). All rights reserved.
 *
 */

int read_script(Sim *s);