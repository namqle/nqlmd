/*
 *  forces.h
 *  nqlmd
 *
 *  Created by Nam Quang Le on 11/26/12.
 *  Copyright 2012 Nam Q. Le (University of Virginia). All rights reserved.
 *
 */

int forces(Sim *s);

void update_virial(Sim *s, int i, int j, double *d_proj);