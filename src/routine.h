/*
 *  routine.h
 *  nqlmd
 *
 *  Created by Nam Quang Le on 11/25/12.
 *  Copyright 2012 Nam Q. Le (University of Virginia). All rights reserved.
 *
 */

#define ROUTINE_ID_INTEGRATE 1
#define INTEGRATE_ID_VERLET 1
#define INTEGRATE_ID_VVERLET 2
#define ROUTINE_ID_BUILD_NBLIST 2

#define ROUTINE_ID_THERMOSTAT 11
#define ROUTINE_ID_BAROSTAT 12
#define ROUTINE_ID_HEAT_FLUX 13
#define ROUTINE_ID_TEMP_PROFILE 14

#define ROUTINE_ID_WRITE_MACRO 91
#define ROUTINE_ID_WRITE_MICRO 92
#define ROUTINE_ID_WRITE_LAMMPS 93
#define ROUTINE_ID_WRITE_CFG 94
#define ROUTINE_ID_WRITE_NMD 95
#define ROUTINE_ID_WRITE_RESTART 96

extern int routine_setup(Sim *s, char args[][MAX_SCRIPT_ARG_LENGTH], int num_args);

extern int routine_run(Sim *s, int num_routine);
