% !TEX TS-program = pdflatexmk
\documentclass[11pt]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PACKAGES                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[top=1in,bottom=1in,right=1in,left=1in]{geometry}

\usepackage{amsmath}

\usepackage{graphicx} % to \includegraphics

\usepackage{color}

\usepackage{multirow} % to allow \multirow cells in \tabular

\usepackage{placeins} % floats cannot cross \FloatBarrier

\usepackage{hyperref}
\hypersetup{
	colorlinks  = true,
	urlcolor    = blue,
	linkcolor   = blue,
	citecolor   = blue
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MACROS                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\angles}[1]{$\langle${#1}$\rangle$}

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BEGIN DOCUMENT          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\title{Operator's Manual: \href{https://bitbucket.org/namqle/nqlmd}{\texttt{nqlmd}}}

\author{Nam~Q.~Le \\ \href{mailto:nam.q.le@gmail.com}{nam.q.le@gmail.com}}

\date{\today}

\maketitle

%\hrule

\tableofcontents

%\vspace{0.25in}
%\hrule

\newpage

\section{Getting started}

\texttt{nqlmd} is code for running classical molecular dynamics (MD) simulations. It's written in C. I wrote it to do my dissertation research, which means (1) it's very bare-bones, and (2) it's geared toward simulations of heat transfer in solid, insulating materials. If you're looking for a general-purpose MD program with good documentation, a large user community, lots of potential functions, and so on, better choices might be \href{http://lammps.sandia.gov}{LAMMPS} or \href{http://www.ks.uiuc.edu/Research/namd/}{NAMD}. However, for very simple simulations, or for situations that require diving into source code, the simplicity of \texttt{nqlmd} might be attractive.

Last warning: I've only used this on Linux and OS X computers. So, for example, the \texttt{makefile} won't work on a Windows machine. It's all just plain C code though, so there should be no trouble compiling ``manually.'' Famous last words.

%\subsection{Features of \texttt{nqlmd}}

\subsection{Getting the code}

The code lives in a public Git repository at \url{https://bitbucket.org/namqle/nqlmd}. If you like Git, you know what to do. Otherwise, you can also find a link there to download a zip file. As of this writing, Bitbucket says that the package is 20.3 MB, which is a dirty lie---it should just be a couple hundred kB.

Either way, you should end up with a directory whose contents are listed on the webpage (and, if all goes well, in \texttt{README.md}).

For Git users, note that there are three branches: \texttt{master}, \texttt{dev}, and \texttt{parallel}. \texttt{master} should be a (relatively) stable version, while \texttt{dev} often has new features, some of which you might find useful. \texttt{parallel} was an attempt to parallelize the code, now on indefinite hiatus. (Help would be welcome\ldots)

\subsection{Compiling it}

Navigate to the \texttt{src/} directory, where you should find a handful of code and header files. There's also a \texttt{makefile}, and if you're lucky, you might be able to simply type \texttt{make}. The code uses the C99 standard, and the makefile assumes \texttt{gcc} as your C compiler.

If that's no good, you can probably compile it yourself. For me, the simplest working command is something like \begin{verbatim}gcc -std=c99 main.c common.c read_script.c md.c forces.c routine.c cleanup.c -o ../bin/nqlmd\end{verbatim} issued in \texttt{src/}. (If you care, Section~\ref{sec:code} has more details about these files and the code itself.)

If all goes well, this produces an executable in \texttt{bin/} called \texttt{nqlmd}.

\subsection{Testing it}

Go to \texttt{examples/example01/}, where you should find an input script (\texttt{example01.script}) along with a few different choices of initial configuration files (\texttt{example01\_0K.init}, \texttt{example01\_40K.init}, and \texttt{example01\_80K.init}). If you peek in the input script, you should see a line that specifies which initial configuration file is actually being used; see Section~\ref{sec:setup}.

In \texttt{examples/example01/}, try \begin{verbatim}../../bin/nqlmd example01 &\end{verbatim} which should set a simulation off running in the background. It takes 14 seconds to finish on my desktop from 2010. The simulation should have written a log  file called \texttt{example01.log}---check this out.

The input script also specifies a routine called \texttt{write\_cfg}, which writes the complete atomic configuration at regular specified intervals. Therefore, you should also see that the simulation generated one \texttt{*.cfg} file every 500 timesteps. These are written in a format compatible with the visualization program Atomeye (\url{http://li.mit.edu/Archive/Graphics/A/}).

\FloatBarrier
\section{Running your own MD simulations}

Once you've compiled and tested \texttt{nqlmd}, you're ready to run your own simulations!

\subsection{Setting up the simulation}
\label{sec:setup}

All you need are two files: (1) an input script to tell \texttt{nqlmd} what to do and (2) a file containing the intial configuration of the system.

\begin{enumerate}
  \item \textbf{The input script.} The name of the script must be \texttt{\angles{something}.script}.\footnote{\textit{Important:} the \angles{something} part of the filename will be used as the ``jobname''. Output files will be saved using the same jobname, with different file extensions. See Section~\ref{sec:output}.} This is a plain text file with one instruction per line, which \texttt{nqlmd} reads sequentially. Each instruction consists of a keyword followed by its corresponding arguments. All recognized keywords are listed in Table~\ref{tab:input_script_keywords}. Note that lines beginning with the `\#' character are ignored as comments.
\begin{verbatim}
# Comments begin with the `#' symbol
<keyword 1>   <argument 1a>
<keyword 2>   <argument 2a> <argument 2b>

# You can have multiple comments to remind yourself what's happening
<keyword 3>   <argument 3a>
...
\end{verbatim}
  Two topics warrant a bit more detail:
  \begin{enumerate}
    \item \emph{Potentials:} Two special keywords are \texttt{pot\_style} and \texttt{pot\_params}, which specify interatomic potential functions. Available interatomic potentials are listed in Table~\ref{tab:potentials}.
      
      Say a simulation contains two atomic types, Si and Ge. Si atoms could be assigned type 1, and Ge atoms assigned type 2. Then three \texttt{pot\_styles} will need to be defined, along with corresponding \texttt{pot\_params}: for types 1--1, 2--2, and 1--2.
    \item \emph{Routines:} Another special keyword is \texttt{routine}, which establishes ``something to be done during the simulation''. A \texttt{routine} might be executed every timestep (e.g., \texttt{integrate}), or at specified intervals of steps. Available \texttt{routine}s are listed in Table~\ref{tab:routines}.
  \end{enumerate}
  \item \textbf{The initial configuration file.} The name of this file is specified in the input script by the keyword \texttt{init\_file}. This file contains the atomic configuration from which the simulation starts, formatted like this: 
\begin{verbatim}
One header line for comments, which is ignored
<total number of atoms, N>
<id1> <type1> <mass1> <x1> <y1> <z1> <vx1> <vy1> <vz1>
<id2> <type2> <mass2> <x2> <y2> <z2> <vx2> <vy2> <vz2>
...
<idN> <typeN> <massN> <xN> <yN> <zN> <vxN> <vyN> <vzN>
\end{verbatim} 
You'll need to generate this file using some kind of external script; as an example, see the Python script \texttt{gen\_crystal.py} provided in \texttt{scripts/}.

The fields are self-explanatory, except perhaps \texttt{id} and \texttt{type}. The \texttt{id} in fact is not used by \texttt{nqlmd}, but can be helpful for post-processing. The \texttt{type} determines interatomic interactions. In a given simulation, the atom types should count up sequentially starting at~1. For how they're used, see the keyword \texttt{pot\_style} in Table~\ref{tab:input_script_keywords}, with more details in Section~\ref{sec:overview}.
\end{enumerate}
For examples of these two files---well, see the examples. In \texttt{examples/}.

\begin{table}[htbp]
  \caption{List of keywords and their arguments for input scripts. Use them roughly in this order.}
  \begin{center}
    \begin{tabular}{l l p{4.0in}}
      keyword & arguments & comments \\
      \hline
      \texttt{sim\_units}      & \angles{unit system}    & Can either be ``\texttt{aap}'' (\AA, amu, ps, eV) or ``\texttt{si}'' (m, kg, s, J). NOTE: pressure is \emph{always} in N\,m$^{-(d-1)}$; $d$ is \# of dimensions. \\
      \\
      \texttt{sim\_dim}        & \angles{\# dimensions}  & Integer number of dimensions to simulate: ``\texttt{1}'', ``\texttt{2}'', or ``\texttt{3}''. Determines the number of arguments expected for \texttt{sim\_box} and \texttt{sim\_pbc}, as well as format of the file specified by \texttt{init\_file}. \\
      \\
      \texttt{sim\_box}        & \angles{$x_\text{min}$} & Lower bound along the $x$ axis of the simulation domain, in length units specified by \texttt{sim\_units}. \\
                               & \angles{$x_\text{max}$} & Corresponding upper bound. \\
                               & \ldots                  & And so on. In total, specify 2~$\times$~\texttt{sim\_dim} numbers. \\
      \\
      \texttt{sim\_pbc}        & \angles{PBC$_x$}        & Can either be ``\texttt{1}'' (on) or ``\texttt{0}'' (off). \\
                               & \ldots                  & Specify a value for each dimension. \\
      \\
      \texttt{group}           & \angles{group id}       & Integer ID for this group of atoms. NOTE: group ID ``\texttt{0}'' (zero) is reserved to encompass ``all atoms''. \\
                               & \angles{$x_\text{min}$} & \multirow{3}{*}{\parbox{4.0in}{Define a region in the same manner as with \texttt{sim\_box}. All atoms found inside this region \emph{at the beginning of the simulation} are considered in this group.}} \\
                               & \angles{$x_\text{max}$} &  \\
                               & \ldots                  &  \\
      \\
      \texttt{init\_file}      & \angles{config file}    & The name of a file containing the initial atomic configuration. \\
      \\
      \texttt{pot\_style}      & \angles{atom type 1}    & \multirow{3}{*}[0.6em]{\parbox{4.0in}{Set the potential \angles{style} to govern interactions between \angles{atom type 1} and \angles{atom type 2}. See Table~\ref{tab:potentials}. }} \\
                               & \angles{atom type 2}    \\
                               & \angles{style}          \\
      \\
      \texttt{pot\_params}     & \angles{parameter 1}    & Format depends on the \texttt{pot\_style}. See Table~\ref{tab:potentials}. \\
                               & \ldots                  & \\
      \\
      \texttt{timestep}        & \angles{timestep}       & Time between steps, in units specified by \texttt{sim\_units}. \\
      \\
      \texttt{set\_routine}    & \angles{routine ID}     & Integer ID that determines the routine's ``place in line'' when the routine list is executed: ``\texttt{0}'', ``\texttt{1}'', ``\texttt{2}'', \ldots. \\
                               & \angles{routine name}   & See Table~\ref{tab:routines} for a list of available routines, along with their expected parameters. \\
                               & \angles{parameter 1}    & First parameter for this routine. \\
                               & \ldots                  & \\
      \\
      \texttt{unset\_routine}  & \angles{routine ID}     & ID of a routine to ``turn off.'' \\
      \\
      \texttt{run\_until}      & \angles{step \#}        & Proceed with MD simulation until the given step \#. NOTE: step numbers persist over simulations within the same script. \\
    \end{tabular}
    \label{tab:input_script_keywords}
  \end{center}
\end{table}

\begin{table}[htbp]
  \caption{List of available interatomic potential styles and their arguments.}
  \begin{center}
  \begin{tabular}{l l p{2.0in} p{2.0in}}
      style & arguments & comments & $U_{i,j}(r_{i,j})$ = \\
      \hline
      \texttt{harmonic}   & \angles{$k^{(2)}$}        & 2nd-order force constant, in [energy]/[length]$^2$& \multirow{3}{2.0in}[0.6em]{\parbox{2.0in}{$$ \begin{cases} \frac{1}{2} k^{(2)} (r_{i,j} - r_\text{eq})^2, & r_{i,j} \leq r_\text{cut} \\ 0, & r_{i,j} > r_\text{cut} \end{cases} $$}} \\
                          & \angles{$r_\text{eq}$}    & Equilibrium distance & \\
                          & \angles{$r_\text{cut}$}   & Interaction cut off distance& \\
      \\                                                  
      \texttt{anharmonic} & \angles{$k^{(2)}$}        & In [energy]/[length]$^2$ & \multirow{5}{2.0in}[1.0em]{\parbox{2.0in}{$$ \begin{cases} \frac{1}{2} k^{(2)} (r_{i,j} - r_\text{eq})^2 \\ + \frac{1}{6} k^{(3)} (r_{i,j} - r_\text{eq})^3 \\ + \frac{1}{24} k^{(4)} (r_{i,j} - r_\text{eq})^4, & r_{i,j} \leq r_\text{cut} \\ 0, & r_{i,j} > r_\text{cut} \end{cases} $$}} \\
                          & \angles{$k^{(3)}$}        & In [energy]/[length]$^3$ & \\
                          & \angles{$k^{(4)}$}        & In [energy]/[length]$^4$ & \\
                          & \angles{$r_\text{eq}$}    & Equilibrium distance & \\
                          & \angles{$r_\text{cut}$}   & Interaction cut off distance & \\
      \\                                                  
      \texttt{lj}         & \angles{$\epsilon$}       & Energy scale & \multirow{3}{2.0in}[1.2em]{\parbox{2.0in}{$$ \begin{cases} 4 \epsilon \left[ \left(\frac{\sigma}{r_{i,j}}\right)^{12} - \left(\frac{\sigma}{r_{i,j}}\right)^6 \right], & r_{i,j} \leq r_\text{cut} \\ 0, & r_{i,j} > r_\text{cut} \end{cases} $$}} \\
                          & \angles{$\sigma$}         & Length scale & \\
                          & \angles{$r_\text{cut}$}   & Interaction cut off distance & 
    \end{tabular}
    \label{tab:potentials}
  \end{center}
\end{table}

\begin{table}[htbp]
  \caption{List of \texttt{routine}s for use in input scripts.}
  \begin{center}
    \begin{tabular}{l l p{4.0in}}
      routine & arguments & comments \\
      \hline
      \texttt{integrate}     & \angles{scheme}                  & Can either be ``\texttt{verlet}'' or ``vverlet'' (velocity Verlet), implemented following Frenkel \& Smit~\cite{Frenkel1996}. \\
                             & \angles{group id}                & \emph{Optional.} If not specified, assumes group ID ``0'' (all atoms). Useful to omit ``wall'' atoms from integration. \\
      \\                                                        
      \texttt{build\_nblist} & \angles{interval}                & \# steps between building neighbor lists (``Verlet lists''~\cite{Frenkel1996}). \\
                             & \angles{radius}                  & Distance inside which to track neighbors. \\
      \\                                                        
      \texttt{barostat}      & \angles{target $p$}              & Target pressure for Berendsen barostat~\cite{Berendsen1984} in N\,m$^{-(d-1)}$. \\
                             & \angles{compressibility}         & Approximate system compressibility in N\,m$^{-(d-1)}$. \\
                             & \angles{time constant}           & Time constant for pressure control in time units. \\
      \\                                                        
      \texttt{heat\_flux}    & \angles{group id}                & Group of atoms to affect. \\
                             & \angles{energy/time}             & Heat current to add using scheme of Jund and Jullien~\cite{Jund1999}. Can be negative to remove heat current. \\
      \\
      \texttt{temp\_profile} & \angles{dimension}               & Can be ``\texttt{1}'', ``\texttt{2}'', or ``\texttt{3}'' (x, y, or z): the dimension along which to scan temperatures \\
                             & \angles{\# of bins}              & Integer \# of bins to spatially discretize in the given dimension \\
                             & \angles{sample interval}         & \# steps between sampling the temperature in each bin \\
                             & \angles{write interval}          & \# steps between writing the average and standard deviation of temperature in each bin, calculated over past samples. Data are written to \texttt{\angles{jobname}.profile}. \\
      \\                                                        
      \texttt{write\_macro}  & \angles{interval}                & \# steps between writing system-wide data to \texttt{\angles{jobname}.log}. \\
      \\                                                        
      \texttt{write\_micro}  & \angles{interval}                & \# steps between writing atomic data to \texttt{\angles{step}.micro}. Anticipate large file sizes! \\
      \\                                                        
      \texttt{write\_lammps} & \angles{interval}                & \# steps between writing atomic data to \texttt{\angles{step}.lammpstrj}, readable by VMD (\url{http://www.ks.uiuc.edu/Research/vmd/}). Anticipate large file sizes! \\
      \\                                                        
      \texttt{write\_cfg}    & \angles{interval}                & \# steps between writing atomic data to \texttt{\angles{step}.cfg}, readable by Atomeye (\url{http://li.mit.edu/Archive/Graphics/A/}). Anticipate large file sizes! \\
    \end{tabular}
    \label{tab:routines}
  \end{center}
\end{table}

\subsection{Running the simulation}
\label{sec:running}

Easy: call the executable with one command line argument, which must be the prefix of your input script's filename. In other words, if your input script is named \texttt{conductivity.script}, run it with \begin{verbatim}<path>/<to>/nqlmd conductivity\end{verbatim} That's it.

\subsection{Simulation output}
\label{sec:output}

In the following, I abbreviate ``prefix of your input script's filename'' as ``jobname''. So, if your input script is \texttt{conductivity.script}, the jobname is \texttt{conductivity}. As you run a simulation, the program will always keep a log written as \texttt{\angles{jobname}.log}. This is the minimum possible output to expect.

Several \texttt{routine}s will cause additional output. A special one, \texttt{write\_macro}, causes the program to write thermodynamic data to \texttt{\angles{jobname}.log} during simulations. Other \texttt{routine}s cause output to new files. For example, the \texttt{routine} called \texttt{temp\_profile} calculates the temperature profile across a specified dimension of the simulation domain, and it records data in \texttt{\angles{jobname}.profile}.

The descriptions of \texttt{routines} in Table~\ref{tab:routines} specifies any output files to expect.

\FloatBarrier
\section{A closer look}
\label{sec:code}

This section takes a quick glance under the hood, which will hopefully facilitate future revisions by myself and others.

\subsection{Overview}
\label{sec:overview}

The code consists of seven parts:
\begin{center}
  \begin{tabular}{l p{3in}}
    \texttt{main}         & Main program control. \\
    \texttt{common}       & Global constants. \texttt{Sim} data structure. \\
    \texttt{read\_script} & Reads \texttt{\angles{jobname}.script}. \\
    \texttt{routine}      & Initializes \texttt{routine}s on setup, and executes them during simulations. \\
    \texttt{md}           & Runs the actual MD simulations. \\
    \texttt{forces}       & Calculates interatomic forces. \\
    \texttt{cleanup}      & Wraps up the job; e.g., prints timing info.
  \end{tabular}
\end{center}
Here's a rough sketch of the relationship among the seven parts:
\begin{center}
  \includegraphics[width=4.0in]{code_overview.png}
\end{center}

The file \texttt{common.h} defines a type of data structure called a \texttt{Sim}. At the beginning of a job, \texttt{main.c} creates an instance \texttt{s} of this structure; at any given moment during execution, \texttt{s} contains all of the information about the job. For example, \texttt{s->N} is the integer number of atoms in the simulation domain, \texttt{s->sim\_dim} is the number of dimensions spanned by the domain, and \texttt{s->atom\_r[i][d]} is the current position of the atom with index \texttt{i}, projected onto dimension \texttt{d} (0 $\to x$, 1 $\to y$, etc.). Likewise, \texttt{s->atom\_v[][]} contains current velocities.

\subsection{Common revisions and how to make them}
\label{sec:checklists}

For some common types of features to add, here are some corresponding checklists of necessary changes to the code. Good luck.

\newenvironment{cozyitemize}
{\vspace{-0.2in}\begin{itemize} 
  \setlength{\itemsep}{0pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt} }
{\end{itemize}}

\begin{center}
  \begin{tabular}{p{2.3in} p{3.9in}}
    If you want to\ldots & Then\ldots \\
    \hline
    Increase max numbers of things (atoms, types, line length, etc.) & 
                                         \begin{cozyitemize}
                                           \item \texttt{common.h}: change corresponding global variables
                                         \end{cozyitemize} \\
    Add a new potential                & \begin{cozyitemize}
                                           \item \texttt{common.h}: add global constants to define a new \texttt{POT\_STYLE\_NAME\_*} and \texttt{POT\_STYLE\_ID\_*}
                                           \item \texttt{read\_script.c}: in \texttt{read\_script()}, add appropriate entries for the \texttt{pot\_style} and \texttt{pot\_params} keywords
                                           \item \texttt{forces.c}: in \texttt{forces()}, add the appropriate case to the switch on \texttt{pot\_style\_id}
                                         \end{cozyitemize} \\
    Add a new routine                  & \begin{cozyitemize}
                                           \item \texttt{routine.h}: add new constant \texttt{ROUTINE\_ID\_*}
                                           \item \texttt{routine.c}: add case to \texttt{routine\_setup()}, and add case to \texttt{routine\_run()}
                                           \item \texttt{common.h}: add any necessary variable declarations to \texttt{Sim\_def}
                                           \item \texttt{common.c}: initialize variables
                                         \end{cozyitemize} \\
    Add a new unit system              & \begin{cozyitemize}
                                           \item \texttt{common.h}: add any necessary conversion factors and physical constants (see below)
                                           \item \texttt{read\_script.c}: add case for the \texttt{sim\_units} keyword
                                           \item \texttt{read\_script.c}: if $E$ units are inconsistent with $M\,L^2\,T^{-2}$, then convert any \texttt{pot\_params} that include energy units (search for \texttt{!strcmp(s->units, "aap")} for an example)
                                           \item \texttt{routine.c}: in \texttt{routine\_run()}, add conversions/calculations to \texttt{BAROSTAT}, \texttt{HEAT\_FLUX}, \texttt{TEMP\_PROFILE}, \texttt{WRITE\_MACRO}, \texttt{WRITE\_MICRO}, and \texttt{WRITE\_CFG}
                                         \end{cozyitemize} \\
  \end{tabular}
\end{center}



\bibliography{/Users/nql6u/Dropbox/Work/bib/namqle_refs}
\bibliographystyle{/Users/nql6u/Dropbox/Work/bib/unabuser}
%\bibliography{/home/nql6u/Dropbox/Work/bib/namqle_refs}
%\bibliographystyle{/home/nql6u/Dropbox/Work/bib/unabuser}



\end{document}
