# nqlmd

A simple molecular dynamics code, written in C.



##  Files

Pretty self-explanatory:

* `README` -- this readme
* `src/` -- source code and makefile
* `bin/` -- where the compiled binary ends up
* `doc/` -- a user's manual
* `examples/`
    * `example01/` -- thermal relaxation of a small LJ Ar cube
    * `example02/` -- thermal conductivity of LJ Ar by direct method
* `scripts/` -- possibly-useful auxiliary scripts



## Compiling

Edit `src/makefile` to fit your system. It's very crude, but right now
it works for me on UVa's Fir cluster, my office iMac, and my Chromebook
with Ubuntu. Then if you're lucky, just run `make`.

The binary `nqlmd` should end up in `bin/`. Doing `make clean` in the 
source directory removes the build products.



## Running

The program looks for one argument; run like `./nqlmd <jobname>`. Then 
it looks for `<jobname>.script`, which is the main list of settings and
commands that the program reads. 

That input script needs to specify an "init" file, which is where the
simulation's initial conditions are specified. This is just a list of
all the atoms in the simulation, and their initial positions and 
velocities (along with id, mass, and type).

The program can write to output files during the simulation; the names
generally look like `<jobname>.<extension>`.

Check out the examples to see how it works.



## Features

* Potentials
    * Harmonic
    * Anharmonic (2nd + 3rd + 4th order)
    * Lennard-Jones
* Integrators
    * Verlet
    * Velocity Verlet
* "Routines"
    * `barostat`: Control system pressure with a Berendsen barostat
    * `write_macro`: Write thermodynamic data (e.g. KE, PE)
    * `write_micro`: Write atomic data (e.g. xyz, velocities)
    * `write_lammps`: Write data for [VMD](http://www.ks.uiuc.edu/Research/vmd/) to read (lammpstrj format)
    * `write_cfg`: Write data for [AtomEye](http://li.mit.edu/Archive/Graphics/A/) to read (CFG format)
    * `heat_flux`: Scale atom energies in a targeted group
    * Can be set and unset during a simulation
* Other stuff
    * Can do 1d, 2d, 3d simulations
    * Can put atoms into groups for routines to use
* Aux scripts
    * Generate initial conditions: positions and velocities
    * Read atomic data for sampling



## To Do

* Thermostat
* Restarts
* Stillinger-Weber potential
* Routines for phonon DOS?
